-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 21, 2015 at 11:04 PM
-- Server version: 5.5.42-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ramro_movie`
--

-- --------------------------------------------------------

--
-- Table structure for table `paypal_log`
--

DROP TABLE IF EXISTS `paypal_log`;
CREATE TABLE IF NOT EXISTS `paypal_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(600) NOT NULL,
  `log` text NOT NULL,
  `posted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Truncate table before insert `paypal_log`
--

TRUNCATE TABLE `paypal_log`;
--
-- Dumping data for table `paypal_log`
--

INSERT INTO `paypal_log` (`id`, `txn_id`, `log`, `posted_date`) VALUES
(1, '6MY89448GR667181T', 'Array\n(\n    [mc_gross] => 3.00\n    [invoice] => 0559206755\n    [protection_eligibility] => Ineligible\n    [address_status] => confirmed\n    [item_number1] => 80609\n    [payer_id] => MCNWYQ9DUAECG\n    [tax] => 0.00\n    [address_street] => 1 Main St\n    [payment_date] => 23:00:03 Apr 18, 2015 PDT\n    [payment_status] => Pending\n    [charset] => windows-1252\n    [address_zip] => 95131\n    [mc_shipping] => 0.00\n    [mc_handling] => 0.00\n    [first_name] => test\n    [address_country_code] => US\n    [address_name] => test buyer\n    [notify_version] => 3.8\n    [custom] => \n    [payer_status] => verified\n    [address_country] => United States\n    [num_cart_items] => 1\n    [mc_handling1] => 0.00\n    [address_city] => San Jose\n    [verify_sign] => An5ns1Kso7MWUdW4ErQKJJJ4qi4-Aq78rYjXYlqTOiYFYhGw1oi.3.wI\n    [payer_email] => raghu.kataria-buyer-1@gmail.com\n    [mc_shipping1] => 0.00\n    [tax1] => 0.00\n    [txn_id] => 6MY89448GR667181T\n    [payment_type] => instant\n    [last_name] => buyer\n    [address_state] => CA\n    [item_name1] => Ramro Movie Subscription\n    [receiver_email] => raghu.katariafacilatitor@gmail.com\n    [quantity1] => 3\n    [pending_reason] => unilateral\n    [txn_type] => cart\n    [mc_gross_1] => 3.00\n    [mc_currency] => USD\n    [residence_country] => US\n    [test_ipn] => 1\n    [transaction_subject] => \n    [payment_gross] => 3.00\n    [ipn_track_id] => e3801fff4f71d\n)\n', '2015-04-18 23:00:11'),
(2, '18H9350095763352Y', 'Array\n(\n    [mc_gross] => 2.00\n    [invoice] => 0623277302\n    [protection_eligibility] => Ineligible\n    [address_status] => confirmed\n    [item_number1] => 40390\n    [payer_id] => MCNWYQ9DUAECG\n    [tax] => 0.00\n    [address_street] => 1 Main St\n    [payment_date] => 23:25:29 Apr 18, 2015 PDT\n    [payment_status] => Pending\n    [charset] => windows-1252\n    [address_zip] => 95131\n    [mc_shipping] => 0.00\n    [mc_handling] => 0.00\n    [first_name] => test\n    [mc_fee] => 0.36\n    [address_country_code] => US\n    [address_name] => test buyer\n    [notify_version] => 3.8\n    [custom] => \n    [payer_status] => verified\n    [business] => raghu.kataria-facilitator@gmail.com\n    [address_country] => United States\n    [num_cart_items] => 1\n    [mc_handling1] => 0.00\n    [address_city] => San Jose\n    [verify_sign] => AiKZhEEPLJjSIccz.2M.tbyW5YFwAUssEwEwjV2.6OFLws.GLQFzK5uk\n    [payer_email] => raghu.kataria-buyer-1@gmail.com\n    [mc_shipping1] => 0.00\n    [tax1] => 0.00\n    [txn_id] => 18H9350095763352Y\n    [payment_type] => instant\n    [last_name] => buyer\n    [address_state] => CA\n    [item_name1] => Ramro Movie Subscription\n    [receiver_email] => raghu.kataria-facilitator@gmail.com\n    [payment_fee] => 0.36\n    [quantity1] => 1\n    [receiver_id] => RBDRQ3M4RQKJG\n    [pending_reason] => paymentreview\n    [txn_type] => cart\n    [mc_gross_1] => 2.00\n    [mc_currency] => USD\n    [residence_country] => US\n    [test_ipn] => 1\n    [transaction_subject] => \n    [payment_gross] => 2.00\n    [ipn_track_id] => c3af3d33470d\n)\n', '2015-04-18 23:25:47'),
(3, '9BF62978YN271144P', 'Array\n(\n    [mc_gross] => 2.00\n    [invoice] => 0925454247\n    [protection_eligibility] => Ineligible\n    [address_status] => confirmed\n    [item_number1] => 34657\n    [payer_id] => MCNWYQ9DUAECG\n    [tax] => 0.00\n    [address_street] => 1 Main St\n    [payment_date] => 02:33:29 Apr 19, 2015 PDT\n    [payment_status] => Pending\n    [charset] => windows-1252\n    [address_zip] => 95131\n    [mc_shipping] => 0.00\n    [mc_handling] => 0.00\n    [first_name] => test\n    [mc_fee] => 0.36\n    [address_country_code] => US\n    [address_name] => test buyer\n    [notify_version] => 3.8\n    [custom] => \n    [payer_status] => verified\n    [business] => raghu.kataria-facilitator@gmail.com\n    [address_country] => United States\n    [num_cart_items] => 1\n    [mc_handling1] => 0.00\n    [address_city] => San Jose\n    [verify_sign] => Ae-XDUZhrxwaCSsmGO9JpO33K7P1AS8zn2ZL0pKXRM.fJ3UhEE245sSq\n    [payer_email] => raghu.kataria-buyer-1@gmail.com\n    [mc_shipping1] => 0.00\n    [tax1] => 0.00\n    [txn_id] => 9BF62978YN271144P\n    [payment_type] => instant\n    [last_name] => buyer\n    [address_state] => CA\n    [item_name1] => Ramro Movie Subscription\n    [receiver_email] => raghu.kataria-facilitator@gmail.com\n    [payment_fee] => 0.36\n    [quantity1] => 1\n    [receiver_id] => RBDRQ3M4RQKJG\n    [pending_reason] => paymentreview\n    [txn_type] => cart\n    [mc_gross_1] => 2.00\n    [mc_currency] => USD\n    [residence_country] => US\n    [test_ipn] => 1\n    [transaction_subject] => \n    [payment_gross] => 2.00\n    [ipn_track_id] => 46fdac8735903\n)\n', '2015-04-19 02:33:37'),
(4, '568064401J265571F', 'Array\n(\n    [mc_gross] => 2.00\n    [invoice] => 1042458436\n    [protection_eligibility] => Ineligible\n    [address_status] => confirmed\n    [item_number1] => 18293\n    [payer_id] => MCNWYQ9DUAECG\n    [tax] => 0.00\n    [address_street] => 1 Main St\n    [payment_date] => 03:43:41 Apr 19, 2015 PDT\n    [payment_status] => Pending\n    [charset] => windows-1252\n    [address_zip] => 95131\n    [mc_shipping] => 0.00\n    [mc_handling] => 0.00\n    [first_name] => test\n    [mc_fee] => 0.36\n    [address_country_code] => US\n    [address_name] => test buyer\n    [notify_version] => 3.8\n    [custom] => \n    [payer_status] => verified\n    [business] => raghu.kataria-facilitator@gmail.com\n    [address_country] => United States\n    [num_cart_items] => 1\n    [mc_handling1] => 0.00\n    [address_city] => San Jose\n    [verify_sign] => AomRS5l2W2xlt2An.GaSrAzpCl-NA.iFIpzqJrWNGKKiMKyO6Fau-.x.\n    [payer_email] => raghu.kataria-buyer-1@gmail.com\n    [mc_shipping1] => 0.00\n    [tax1] => 0.00\n    [txn_id] => 568064401J265571F\n    [payment_type] => instant\n    [last_name] => buyer\n    [address_state] => CA\n    [item_name1] => Ramro Movie Subscription\n    [receiver_email] => raghu.kataria-facilitator@gmail.com\n    [payment_fee] => 0.36\n    [quantity1] => 1\n    [receiver_id] => RBDRQ3M4RQKJG\n    [pending_reason] => paymentreview\n    [txn_type] => cart\n    [mc_gross_1] => 2.00\n    [mc_currency] => USD\n    [residence_country] => US\n    [test_ipn] => 1\n    [transaction_subject] => \n    [payment_gross] => 2.00\n    [ipn_track_id] => a649b42041a86\n)\n', '2015-04-19 03:47:34'),
(5, '2H344232W6505034E', 'Array\n(\n    [mc_gross] => 2.00\n    [invoice] => 1046024180\n    [protection_eligibility] => Ineligible\n    [address_status] => confirmed\n    [item_number1] => 91517\n    [payer_id] => MCNWYQ9DUAECG\n    [tax] => 0.00\n    [address_street] => 1 Main St\n    [payment_date] => 03:46:44 Apr 19, 2015 PDT\n    [payment_status] => Pending\n    [charset] => windows-1252\n    [address_zip] => 95131\n    [mc_shipping] => 0.00\n    [mc_handling] => 0.00\n    [first_name] => test\n    [mc_fee] => 0.36\n    [address_country_code] => US\n    [address_name] => test buyer\n    [notify_version] => 3.8\n    [custom] => \n    [payer_status] => verified\n    [business] => raghu.kataria-facilitator@gmail.com\n    [address_country] => United States\n    [num_cart_items] => 1\n    [mc_handling1] => 0.00\n    [address_city] => San Jose\n    [verify_sign] => A--8MSCLabuvN8L.-MHjxC9uypBtAuWdRXJULgVuZrFBoqNBnqL9IRjt\n    [payer_email] => raghu.kataria-buyer-1@gmail.com\n    [mc_shipping1] => 0.00\n    [tax1] => 0.00\n    [txn_id] => 2H344232W6505034E\n    [payment_type] => instant\n    [last_name] => buyer\n    [address_state] => CA\n    [item_name1] => Ramro Movie Subscription\n    [receiver_email] => raghu.kataria-facilitator@gmail.com\n    [payment_fee] => 0.36\n    [quantity1] => 1\n    [receiver_id] => RBDRQ3M4RQKJG\n    [pending_reason] => paymentreview\n    [txn_type] => cart\n    [mc_gross_1] => 2.00\n    [mc_currency] => USD\n    [residence_country] => US\n    [test_ipn] => 1\n    [transaction_subject] => \n    [payment_gross] => 2.00\n    [ipn_track_id] => fa9d9bb56d011\n)\n', '2015-04-19 03:49:24');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_transaction`
--

DROP TABLE IF EXISTS `paypal_transaction`;
CREATE TABLE IF NOT EXISTS `paypal_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `invoice` text NOT NULL,
  `payment_for` varchar(50) NOT NULL COMMENT 'Subscription | Premium',
  `movie_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `hash_payment_id` text NOT NULL,
  `approved` int(5) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Truncate table before insert `paypal_transaction`
--

TRUNCATE TABLE `paypal_transaction`;
--
-- Dumping data for table `paypal_transaction`
--

INSERT INTO `paypal_transaction` (`id`, `user_id`, `invoice`, `payment_for`, `movie_id`, `payment_id`, `hash_payment_id`, `approved`, `created_at`) VALUES
(1, 45911, '5539fb93919db', 'premium', 16, 'PAY-10L96825HH3638346KU47XFI', '190a0ea629798bdf7e47e64286d38dc3', 1, '2015-04-24 08:15:15'),
(3, 45911, '553b1ddf02fa1', 'premium', 15, 'PAY-9KR07053EJ5934722KU5R3YA', '3b9168c796fc48f3b2dc095b281bcfa5', 1, '2015-04-25 04:53:50'),
(4, 45920, '554622749a1a8', 'premium', 15, 'PAY-8DD46872CX2376040KVDCE5Q', 'e1058be66a7ecd45bd260d301735c116', 0, '2015-05-03 13:28:20'),
(5, 45910, '554b93c477827', 'premium', 15, 'PAY-4B550557CB2029615KVFZHRQ', 'e3d6a882235eb3792cfcce846578ff45', 0, '2015-05-07 16:33:08'),
(6, 45910, '5550425522eee', 'premium', 15, 'PAY-43L01404CL856463VKVIEEVY', '9da4284861c64244b39b5cd8a30efa84', 0, '2015-05-11 05:47:00'),
(7, 45910, '5566950c1548e', 'premium', 33, 'PAY-2YF09657EF366871MKVTJKDQ', 'cde22f17f7ec8e4cb3e645e9a2398536', 0, '2015-05-28 04:09:47'),
(8, 45917, '556ab908853c3', 'premium', 28, 'PAY-5BP3133372409112VKVVLSCY', '1c73994d426309878d26fe2592913c91', 0, '2015-05-31 07:32:24'),
(9, 45917, '556c8dafc3117', 'premium', 33, 'PAY-0UU145885V314224UKVWI3MQ', '4b43da6b27520c3b2c34c0ca116ace7a', 0, '2015-06-01 16:51:59'),
(10, 45921, '556eed9c428a6', 'premium', 50, 'PAY-6D659725R19964845KVXO3HY', 'ac2d79f459081fea7fd89b31c6d99270', 0, '2015-06-03 12:05:48'),
(11, 45919, '557157b11c2a5', 'premium', 54, 'PAY-8G043080R1126863JKVYVPMY', 'cadf54cb53296eee4aaefa31dee6345e', 1, '2015-06-05 08:02:56'),
(12, 45911, '5574771c41501', 'premium', 28, 'PAY-5SX037558Y051444YKV2HOHQ', 'aab0bd12e56fccbc8f9df141601a32fc', 0, '2015-06-07 16:53:47'),
(13, 45914, '5577d6ddd6edd', 'premium', 56, 'PAY-83Y786152W6839153KV35NYI', '7150199520fbc49946e0a022f6b804c7', 0, '2015-06-10 06:19:09'),
(14, 45910, '5577d783dafe6', 'premium', 53, 'PAY-5M069118CM6571121KV35PBI', 'cda5cf93fbe555c362764dbee858334a', 0, '2015-06-10 06:21:55'),
(15, 45896, '5577e1978daa2', 'premium', 16, 'PAY-38856439PG772793XKV36DHA', 'dcce11c9ee75bdeee33550eb2f867c5f', 0, '2015-06-10 07:04:55'),
(16, 45914, '557e7be20bc5d', 'premium', 55, 'PAY-7C625363Y9556641RKV7HXZI', '4bcb8c75b50035551bc2026f0bf1ad19', 0, '2015-06-15 07:16:49'),
(17, 45915, '55995d02d99db', 'premium', 52, 'PAY-1F947878VP963441BKWMV2BI', '6eaec7e92989f28a6a9d56d41fe6d113', 0, '2015-07-05 16:36:18'),
(18, 45919, '559a670b2b141', 'premium', 51, 'PAY-6WH10031NK3522629KWNGODI', 'fbcffc8f82c9699ba34c685f72c7e28c', 0, '2015-07-06 11:31:22'),
(19, 45910, '559c22f194a64', 'premium', 49, 'PAY-7H786805T09396933KWOCF4Y', '5335617ec904a81bfa7b364e023cb2ee', 0, '2015-07-07 19:05:21'),
(20, 45917, '559cd133033dd', 'premium', 55, 'PAY-2NY71291PA0351021KWONCNA', '654505826400de75c3fb4bb5cf756c0a', 0, '2015-07-08 07:28:50'),
(21, 45917, '559f6357302f3', 'premium', 24, 'PAY-78S39480FA172093MKWPWGWI', '74147092b09a3458076dd5716c093ffa', 0, '2015-07-10 06:16:54'),
(22, 45929, '55af2c076f356', 'premium', 38, 'PAY-3HK465752E366933NKWXSYCI', '35368aa3254f3c43d095e8ab79d62d44', 0, '2015-07-22 05:37:10'),
(23, 45929, '55af2c0a4edb1', 'premium', 38, 'PAY-2MS613454D406243BKWXSYCY', '66a31c39f547fbb9eb7516de5a065c85', 0, '2015-07-22 05:37:14'),
(24, 45929, '55af2c4c456cd', 'premium', 38, 'PAY-8K177652B5908014DKWXSYTI', '9956ccc58f797be8bdb321f69f714b0d', 0, '2015-07-22 05:38:20'),
(25, 45935, '55c33614555c3', 'premium', 25, 'PAY-08U28570HP953594DKXBTMFQ', '2f1b1e0c897d56e618dfeb5b0aee9239', 0, '2015-08-06 10:25:24'),
(26, 45935, '55c3361a77408', 'premium', 25, 'PAY-5VA4392685744973FKXBTMGY', '107d6b0d833b137a9ef093f8e949d74f', 0, '2015-08-06 10:25:30');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(300) NOT NULL,
  `trasaction_id` varchar(600) NOT NULL,
  `log_id` int(10) NOT NULL,
  `product_id` varchar(300) NOT NULL,
  `product_name` varchar(300) NOT NULL,
  `product_quantity` varchar(300) NOT NULL,
  `product_amount` varchar(300) NOT NULL,
  `payer_fname` varchar(300) NOT NULL,
  `payer_lname` varchar(300) NOT NULL,
  `payer_address` varchar(300) NOT NULL,
  `payer_city` varchar(300) NOT NULL,
  `payer_state` varchar(300) NOT NULL,
  `payer_zip` varchar(300) NOT NULL,
  `payer_country` varchar(300) NOT NULL,
  `payer_email` text NOT NULL,
  `payment_status` varchar(300) NOT NULL,
  `posted_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Truncate table before insert `purchases`
--

TRUNCATE TABLE `purchases`;
--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `invoice`, `trasaction_id`, `log_id`, `product_id`, `product_name`, `product_quantity`, `product_amount`, `payer_fname`, `payer_lname`, `payer_address`, `payer_city`, `payer_state`, `payer_zip`, `payer_country`, `payer_email`, `payment_status`, `posted_date`) VALUES
(3, '0623277302', '18H9350095763352Y ', 2, '40390', 'Ramro Movie Subscription', '1', '2.00', 'Saru', 'Subedi', 'Bhairahawa', 'Lumbini', 'Rupandehi', '123456', 'US', 'saru.subedi@yahoo.com', 'pending', '2015-04-18 23:25:05'),
(2, '0559206755', '6MY89448GR667181T ', 1, '80609', 'Ramro Movie Subscription', '3', '1.00', 'Raghu', 'Cdh', 'New Baneswor', 'Kathmandu', 'Bagmati', '123456', 'US', 'raghav_nu@yahoo.com', 'pending', '2015-04-18 22:59:24'),
(4, '0925454247', '9BF62978YN271144P ', 3, '34657', 'Ramro Movie Subscription', '1', '2.00', 'Raghu', 'Cdh', 'New Baneswor', 'Kathmandu', 'Bagmati', '123456', 'NP', 'raghav_nu@yahoo.com', 'pending', '2015-04-19 02:30:59'),
(5, '1042458436', '568064401J265571F ', 4, '18293', 'Ramro Movie Subscription', '1', '2.00', 'Raghu', 'Cdh', 'New Baneswor', 'Kathmandu', 'Bagmati', '123456', 'NP', 'raghav_nu@yahoo.com', 'pending', '2015-04-19 03:42:48'),
(6, '1046024180', '2H344232W6505034E ', 5, '91517', 'Ramro Movie Subscription', '1', '2.00', 'Mohan', 'Cdh', 'New Baneswor', 'Kathmandu', 'Bagmati', '123456', 'NP', 'raghav_nu@yahoo.com', 'pending', '2015-04-19 03:46:11');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie`
--

DROP TABLE IF EXISTS `ramro_movie`;
CREATE TABLE IF NOT EXISTS `ramro_movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `trailer_link` varchar(255) CHARACTER SET latin1 NOT NULL,
  `full_link` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'Original Video Link',
  `movie_type` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'Pay Watch | Subscription | Free',
  `price` varchar(80) CHARACTER SET latin1 NOT NULL,
  `movie_tag` varchar(255) CHARACTER SET latin1 NOT NULL,
  `cast` varchar(255) CHARACTER SET latin1 NOT NULL,
  `director` varchar(90) CHARACTER SET latin1 NOT NULL,
  `music` varchar(80) CHARACTER SET latin1 NOT NULL,
  `language` varchar(80) CHARACTER SET latin1 NOT NULL,
  `subtitle` varchar(255) CHARACTER SET latin1 NOT NULL,
  `url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `viewed` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `meta_author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66 ;

--
-- Truncate table before insert `ramro_movie`
--

TRUNCATE TABLE `ramro_movie`;
--
-- Dumping data for table `ramro_movie`
--

INSERT INTO `ramro_movie` (`id`, `cat_id`, `image`, `title`, `details`, `trailer_link`, `full_link`, `movie_type`, `price`, `movie_tag`, `cast`, `director`, `music`, `language`, `subtitle`, `url`, `viewed`, `status`, `user_id`, `meta_author`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(7, 2, 'Asset/uploads/movie/150421091_star_pt.png', 'Star', '<p>Mahendra Budhathoki presents Nepali Movie STAR<br /><br />Starring: Niraj Baral, Shyam Ghimire, Sumina Ghimire, Ramesh Budhathoki, Ganesh Upreti, Rupa Khanal.<br />Make Up: Gopi Sapkoti<br />Background Music: Binaya K. TU.<br /> Production Controller: Abhimanyu KC.<br />Distributor: Karan Shrestha (Crezy)<br />Lyrics: Deeps Shaha, Dayaram Paudel, Mahendra Budhathoki, Manisha Budhathoki.<br />Music: Riyaz Siwakoti, Arjun Pokharel, Sarbajyaman Shakya.<br />Singer: Sambhujit Baskota, Sarbajyaman Skakya, Aakanchhya Basyal, Rajani Rai, Shanti Gurung, Uzzal Bhandari.<br />Choreography: Kabiraj Gahatraj, Shiva BK, Suraj Chand.<br />Action: Asta Maharjan.<br />Co-producer: Santosh Neupane, Sasha Ghimire.<br />Assist. Director: B.P. Sharma<br />Executive Producer: Laxman Subedi.<br />Producer: Manisha Budhathoki, Shyam Shrestha, Damodar Gautam.<br />Story/Script/Director: Mahendra Budhathoki.<br />Copyright Highlights Nepal Pvt. Ltd.</p>', '134178552', '135362439', 'free', '', 'Sexy, Nepali Movie,Sumnima Ghimire, Star', 'Niraj Baral, Shyam Ghimire, Sumina Ghimire, Ramesh Budhathoki, Ganesh Upreti, Rupa Khanal', 'Mahendra Budhathoki', 'Riyaz Siwakoti, Arjun Pokharel, Sarbajyaman Shakya', 'Nepali', 'NA', 'Star', 178, 1, 45896, '', 'Star, Nepali movie,Sumnima Ghimire,Hot Nepali Movie,Sexy Nepali Movie', '<p>Mahendra Budhathoki presents Nepali Movie STAR, Starring: Sumina Ghimire, Ramesh Budhathoki, Ganesh Upreti, Rupa Khanal</p>', '2015-08-21 13:26:38', '2015-08-21 13:26:38'),
(11, 2, 'Asset/uploads/movie/150421096_Saurya_pt.jpg', 'Sourya', '<p>The action movie by Hakim Entertaimnet, &lsquo;Saurya&rsquo; with sub-title Rise of Hero is a 2.5 hours long movie. The movie features the screenplay of Subash Singh Basnet / Markanday Dipbim Basnet, cinematography of Rameshowr Karki, Editing of Chandra Dutta / Bishal Koirala, action of Shankar Maharjan and produced by Surendra Gurung &lsquo;Hakim&rsquo;. The movie is the debut movie of Hema Shrestha and Anup Bikram Shahi.</p>', '135240398', '135362438', 'free', '', 'Rajesh hamal,Sourya,Nepali,History', 'Rajesh Hamal, Saugat Malla, Hema Shrestha, Anup Bikram Shahi, Ravi Basnet, Puskar Bhatt etc.', 'Markanday Dipbim Basnet', 'Unknown', 'Nepali', 'NA', 'Sourya', 191, 1, 45896, '', '', '', '2015-08-03 11:30:37', '2015-08-03 11:30:37'),
(13, 3, 'Asset/uploads/movie/150712091390722_1421287541419799_823670483_n.jpg', 'Jhelee', '<p>AMAR RATNA PRODUCTION PRESNETS JHELI<br />Dress Man: Lok Bahadur Budhathoki<br />Hair Dresser: Mejha Shahi<br />Still Photographer: Udit Sapkota<br />Make Up: Susma Joshi, Sarita Pradhan<br />Assist. Director: Shisir Chalise<br />Assist. Cinematography: Rabin Neupane<br />Title/Promo: Shahil Khan / Sunil Thapa<br />Chief Assist. Director: Parichhed Sen<br />Lyrics: Dayaram Dhahal, Muskan Dhakal<br />Singer: Sanup Paudel, Pratap Lama, Mina Singh<br />Fight: Chandra Panth, Ram Gurung, Soneam Lama<br />Story: Bheshman Shrestha<br />Editor: Roshan Lamichhane<br />Producer: Ratna Maya Shrestha, Ram Prasad Maharjan, Sunder Bahadur Khatri<br />&copy; Highlights Nepal Pvt. Ltd.</p>', '134177549', '138432405', 'free', '', 'Surbina Karki, Dipasha BC, Hot Nepali Movie, Jheli', 'Surbina Karki, Anil Thapa, Dipasha BC', 'Kamal Khanal', 'Suryakiran Lama', 'Nepali', 'NA', 'Jhelee', 256, 1, 45896, '', '', '<p>AMAR RATNA PRODUCTION PRESNETS JHELI</p>', '2015-09-06 09:14:40', '2015-09-06 09:14:40'),
(14, 2, 'Asset/uploads/movie/150424113-Kanchi_matyang_tyang_pt.png', 'Kanchi Matyang Tyang', '<p>The movie &lsquo;Kanchi Matyang Tyang&rsquo; is a comedy movie by director Puran Thapa. Well known in small screen films, Puran, had debuted in a big screen movie through this comedy movie. The leading actress of the movie, Sarika KC, has also debuted as an actress in the movie. The movie is produced by Basanta Raj Tamang. The music of the movie is composed by Bikash Chaudhary and Tanka Budhathoki. Choreography of the songs are done by Kamal Rai and Suren BAsel and action by NB Maharjan.</p>', '135229840', '', 'premium', '2', 'Comedy, Nepali Movie, Puran Thapa,Kanchi Matyang Tyang', 'Jaya Kishan Basnet, Sarika KC', 'Puran Thapa', 'Bikash Chaudhary and Tanka Budhathoki', 'Nepali', 'NA', 'Kanchi-Matyang-Tyang', 380, 1, 45896, '', '', '<p>Kanchi matyang tyang, Nepal Comedy Movie, Best Comedy Nepali Movie Comedy Nepali Cinema</p>', '2015-08-21 11:30:42', '2015-08-21 11:30:42'),
(15, 1, 'Asset/uploads/movie/150421092-6-ekan-6_po.jpg', '6 ekan 6', '<p>Nepali movie &lsquo;Chha Ekan Chha&rsquo; is a comedy film by the well known comedy actors of television. The lead actors in the movie are the well known comedy actors of &lsquo;Tito Satya&rsquo;, &lsquo;Meri Bassai&rsquo; and &lsquo;Jire Khursani&rsquo;.&nbsp; The movie is produced by the makers of &lsquo;Tito Satya&rsquo; &ndash; Deepak Raj Giri, Deepa Shree Niroula and Nirmal Sharma (Gaida). The film is directed by Dinesh DC. The lead actress of the movie, Neeta Dhungana, is considered an actress with a lot of potential in the Nepali film industry.</p>\r\n<p>The film makers has aggressively used online and offline media to promote the movie. All the three popular comedy serials had been promoting the movie and the producers have also travelled all over Nepal to promote the movie.</p>\r\n<p><strong>Movie Profile</strong></p>\r\n<p><!-- [if gte mso 9]><xml><w:WordDocument><w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel><w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery><w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery><w:DocumentKind>DocumentNotSpecified</w:DocumentKind><w:DrawingGridVerticalSpacing>7.8</w:DrawingGridVerticalSpacing><w:View>Normal</w:View><w:Compatibility><w:DontGrowAutofit/><w:BalanceSingleByteDoubleByteWidth/><w:DoNotExpandShiftReturn/></w:Compatibility><w:Zoom>0</w:Zoom></w:WordDocument></xml><![endif]--></p>\r\n<p><!-- [if gte mso 9]><xml><w:LatentStyles DefLockedState="false"  DefUnhideWhenUsed="true"  DefSemiHidden="true"  DefQFormat="false"  DefPriority="99"  LatentStyleCount="156" ><w:LsdException Locked="false"  Priority="99"  Name="Normal" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 1" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 6" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 7" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 8" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="heading 9" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 1" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 6" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 7" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 8" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index 9" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 1" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 6" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 7" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 8" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toc 9" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Normal Indent" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="footnote text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="annotation text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="header" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="footer" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="index heading" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="caption" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="table of figures" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="envelope address" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="envelope return" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="footnote reference" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="annotation reference" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="line number" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="page number" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="endnote reference" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="endnote text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="table of authorities" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="macro" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="toa heading" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Bullet" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Number" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Bullet 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Bullet 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Bullet 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Bullet 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Number 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Number 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Number 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Number 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Title" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Closing" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Signature" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Default Paragraph Font" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text Indent" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Continue" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Continue 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Continue 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Continue 4" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="List Continue 5" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Message Header" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Subtitle" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Salutation" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Date" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text First Indent" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text First Indent 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Note Heading" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text Indent 2" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Body Text Indent 3" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Block Text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Hyperlink" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="FollowedHyperlink" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Strong" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Emphasis" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Document Map" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Plain Text" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="E-mail Signature" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Normal (Web)" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Acronym" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Address" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Cite" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Code" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Definition" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Keyboard" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Preformatted" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Sample" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Typewriter" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="HTML Variable" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Normal Table" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="annotation subject" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="No List" ></w:LsdException><w:LsdException Locked="false"  Priority="99"  Name="Balloon Text" ></w:LsdException></w:LatentStyles></xml><![endif]--></p>\r\n<p>Nepali&nbsp;movie&nbsp;&ndash;&nbsp;6&nbsp;Ekan&nbsp;6</p>\r\n<p>Release&nbsp;date&nbsp;&ndash;&nbsp;Jnuary&nbsp;10,&nbsp;2014</p>\r\n<p>Starring&nbsp;&ndash;&nbsp;Deepak&nbsp;Raj&nbsp;Giri,&nbsp;Shiva&nbsp;Hari&nbsp;Poudel,&nbsp;Jitu&nbsp;Nepal,&nbsp;Sitaram&nbsp;Kattel,&nbsp;Kedar&nbsp;Ghimire,&nbsp;Neeta&nbsp;Dhungana&nbsp;etc.</p>\r\n<p>Director&nbsp;&ndash;&nbsp;Dinesh&nbsp;D.C.</p>\r\n<p>Story&nbsp;/&nbsp;script&nbsp;&ndash;&nbsp;Deepak&nbsp;Raj&nbsp;Giri</p>\r\n<p>Cinematographer&nbsp;&ndash;&nbsp;Purushottam&nbsp;Pradhan</p>\r\n<p>Music&nbsp;composer&nbsp;&ndash;&nbsp;Sambhujit&nbsp;Baskota,&nbsp;Rajan&nbsp;Ishan</p>\r\n<p>Banner&nbsp;&ndash;&nbsp;Aama&nbsp;Saraswoti&nbsp;Gitadevi&nbsp;Films&nbsp;Pvt.&nbsp;Ltd.</p>\r\n<p>Producer&nbsp;&ndash;&nbsp;Deepak&nbsp;Raj&nbsp;Giri,&nbsp;Deepa&nbsp;Shree&nbsp;Niraula,&nbsp;Nirmal&nbsp;Sharma</p>\r\n<p>Writer&nbsp;&ndash;&nbsp;Deepak&nbsp;Raj&nbsp;Giri</p>\r\n<p>Choreographer&nbsp;&ndash;&nbsp;Kabiraj&nbsp;Gahatraj,&nbsp;Ghambir&nbsp;Bista</p>\r\n<p>Action&nbsp;&ndash;&nbsp;NB&nbsp;Maharjan</p>\r\n<p>Singers&nbsp;&ndash;&nbsp;Hari&nbsp;Bansha&nbsp;Acharya,&nbsp;Raj&nbsp;Sigdel,&nbsp;Kiran&nbsp;KC,&nbsp;Rajan&nbsp;Ishan,&nbsp;Durga&nbsp;Priyar,&nbsp;Yubaraj&nbsp;Chaulagain,&nbsp;Dinesh&nbsp;DC</p>\r\n<p>Lyrics&nbsp;&ndash;&nbsp;Bhupen&nbsp;Khadka,&nbsp;Deepak&nbsp;Raj&nbsp;Giri</p>\r\n<p>Post&nbsp;Production&nbsp;&ndash;&nbsp;Cine&nbsp;Zone</p>\r\n<p>Background&nbsp;music&nbsp;&ndash;&nbsp;Alisa&nbsp;Karki</p>\r\n<p>Editing&nbsp;&ndash;&nbsp;Ananta&nbsp;Ghimire</p>\r\n<p class="p0" style="margin-bottom: 0pt; margin-top: 0pt;">Makeup&nbsp;&ndash;&nbsp;Bipana&nbsp;Basnet</p>', '135223690', '135350933', 'free', '', '6 Ekan 6,Nepali Comedy, Movie,2014', 'Deepak Raj Giri, Shiva Hari Poudel, etc', 'Dinesh DC', 'Sambhujit Baskota, Rajan Ishan', 'Nepali', 'NA', '6-ekan-6', 542, 1, 45896, '', '', '', '2015-08-03 10:06:43', '2015-08-03 10:06:43'),
(16, 2, 'Asset/uploads/movie/1505110759740_502412679800555_1587787846_n.jpg', 'Premika', '<p>Premika is a modern day romatic tale of a cople. Sharad and Mansi are lovers from childhood. One day, Mansi tells Sharad that she is going to abroad for further studies. This breaks the heart of Sharad and he insists on not letting Mansi go. No matter what he does, but Mansi leaves him. Suddenly, they meet again and Sharad demands the truth. The later part of the story unfolds the truth why Mansi left him in the first place.</p>\r\n<p>Shubha Shanti Film Makers Presents! Nepali Movie: PREMIKA<br />Starring: &nbsp; &nbsp; &nbsp; Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.<br />Make Up: &nbsp; &nbsp; &nbsp;Rabi KC.<br />Hair Dresser: Smriti Shrestha.<br />Chief Assist. &nbsp;Director: Ramchandra Rimal.<br />Production Controller: Rabi Thapa.<br />Motion Graphics: Gautam Raj Khadka.<br />Dubbing : Manoj Rajbahak.<br />Background Music: Alish Karki.<br />Still Photography: Kanchan Shahi.<br />Singer: Suny Sunam, Pratima Sunam, Manisha Pokharel, Dinesh Gautam, Prakash Timalsina.<br />Choreography: Ramji Lamichhane<br />Lyrics: Promod Dhungana.<br />Cinematography: Durga Paudel.<br />Music: Dinesh Sunam.<br /> Script: Sushil Pokharel.<br />Editor: Bipin Malla<br />Co-producer: Romil/Sarika/Avinash<br />Producer: Rabi Acharya.<br />Director: Ramji Lamichhane.<br />Copyright Highlights Nepal Pvt. Ltd.</p>', '138434821', '138434821', 'premium', '2', 'Nepali Movie, Comedy, Action,Drama,Romantic', 'Jharana Thapa, Suman Singh', 'Ramji Lamichhane', 'Dinesh Sunam', 'English', 'English', 'Premika', 180, 1, 45896, '', '', '<p>Premika is a modern day romatic tale of a cople. Sharad and Mansi are lovers from childhood. One day, Mansi tells Sharad that she is going to abroad for further studies.</p>', '2015-09-06 09:17:20', '2015-09-06 09:17:20'),
(17, 3, 'Asset/uploads/movie/15052707aago-2.jpg', 'Aago 2', '<div id="stcpDiv" style="position: absolute; top: -1999px; left: -1988px;">Aago 2 is a new nepali movie depicting the political situation of Nepal. There had been a a lot of revolutionary changes in Nepal since the last decade but non of the changes had made the life of Nepalese easy instead the life of Nepalese people is getting worse day by day. This is what Aago 2 is presenting to the Nepali audience.<br /> - See more at: http://nepalimovieworld.blogspot.com/2015/01/aago-2.html#sthash.Kyu2Uh7s.dpuf</div>\r\n<div id="stcpDiv" style="position: absolute; top: -1999px; left: -1988px;">Aago 2 is a new nepali movie depicting the political situation of Nepal. There had been a a lot of revolutionary changes in Nepal since the last decade but non of the changes had made the life of Nepalese easy instead the life of Nepalese people is getting worse day by day. This is what Aago 2 is presenting to the Nepali audience.<br /> - See more at: http://nepalimovieworld.blogspot.com/2015/01/aago-2.html#sthash.Kyu2Uh7s.dpuf</div>\r\n<p style="text-align: justify;">Aago 2 is a new nepali movie depicting the political situation of Nepal. There had been a a lot of revolutionary changes in Nepal since the last decade but non of the changes had made the life of Nepalese easy instead the life of Nepalese people is getting worse day by day.</p>\r\n<p class="MsoNoSpacing" style="text-align: justify;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Aago-2 is</span><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> a about newspaper named &lsquo;Aago&rsquo; whose Editor in Chief is Amar and Sonu is a news reporter. The film attempts to present the current political status of Nepal, and role of a journalist to take country to the right path.</span></p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p class="MsoNoSpacing" style="text-align: justify;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Sonu is a news reporter of a revolutionary newspaper &ldquo;Aago&rdquo;. Amar (Sushil Chhetri) is the uncle (mama) of Sonu and also editor in chief of the same newspaper. Sonu falls in love with a girl, who happens to be the daughter of corrupt political leader, Bhawani Das. Surya is another important figure in the movie, who gave everything in the revolutionary batter of the country, but was given nothing in return. The Aago newspaper starts writing all the negative things about the political leaders, which creates tension between leaders. They attempt to bribe the newspaper, but fails. At last, they blast the newspaper office. In order to take revenge Surya, Amar, Sonu and his girlfriend starts a new force called National Public Force.</span></p>\r\n<p style="text-align: justify;">&nbsp;</p>', '134177545', '', 'premium', '2', 'Aago', 'Sushil Chhetri, Ganesh Giri, Sarika Ghimire, Priya Rijal, Kisan Sunwar', 'Madan Ghimire', 'Dinesh Sunam', 'Nepali', 'English', 'Aago-2', 58, 1, 45896, '', '', '<p>Latest Nepali Movie, nepali cinema aago, aago 2 watch nepali full movie for free</p>', '2015-08-21 11:44:06', '2015-08-21 11:44:06'),
(18, 3, 'Asset/uploads/movie/15052707letter-new-film-released-october-5-2012.jpg', 'Letter', '<p>Shubha Shanti Film Makers Presents, Nepali Movie: PREMIKA<br />Starring: &nbsp; &nbsp; &nbsp; Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.<br />Make Up: &nbsp; &nbsp; &nbsp;Rabi KC.<br />Hair Dresser: Smriti Shrestha.<br />Chief Assist. &nbsp;Director: Ramchandra Rimal.<br />Production Controller: Rabi Thapa.<br />Motion Graphics: Gautam Raj Khadka.<br />Dubbing : Manoj Rajbahak.<br />Background Music: Alish Karki.<br />Still Photography: Kanchan Shahi.<br />Singer: Suny Sunam, Pratima Sunam, Manisha Pokharel, Dinesh Gautam, Prakash Timalsina.<br />Choreography: Ramji Lamichhane<br />Lyrics: Promod Dhungana.<br />Cinematography: Durga Paudel.<br />Music: Dinesh Sunam.<br /> Script: Sushil Pokharel.<br />Editor: Bipin Malla<br />Co-producer: Romil/Sarika/Avinash<br />Producer: Rabi Acharya.<br />Director: Ramji Lamichhane.<br />Copyright Highlights Nepal Pvt. Ltd.</p>', '135247320', '', 'premium', '', 'Letter', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Letter', 38, 1, 45896, '', '', '', '2015-08-03 13:33:19', '2015-08-03 13:33:19'),
(19, 2, 'Asset/uploads/movie/15052707ho-yehi-maya-ho_movie_poster-3_thumb.jpg', 'Ho yehi maya ho', '<p>Shubha Shanti Film Makers Presents! Nepali Movie: PREMIKA<br />Starring: &nbsp; &nbsp; &nbsp; Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.<br />Make Up: &nbsp; &nbsp; &nbsp;Rabi KC.<br />Hair Dresser: Smriti Shrestha.<br />Chief Assist. &nbsp;Director: Ramchandra Rimal.<br />Production Controller: Rabi Thapa.<br />Motion Graphics: Gautam Raj Khadka.<br />Dubbing : Manoj Rajbahak.<br />Background Music: Alish Karki.<br />Still Photography: Kanchan Shahi.<br />Singer: Suny Sunam, Pratima Sunam, Manisha Pokharel, Dinesh Gautam, Prakash Timalsina.<br />Choreography: Ramji Lamichhane<br />Lyrics: Promod Dhungana.<br />Cinematography: Durga Paudel.<br />Music: Dinesh Sunam.<br /> Script: Sushil Pokharel.<br />Editor: Bipin Malla<br />Co-producer: Romil/Sarika/Avinash<br />Producer: Rabi Acharya.<br />Director: Ramji Lamichhane.<br />Copyright Highlights Nepal Pvt. Ltd.</p>', '135247310', '', 'premium', '', 'Ho yehi maya ho', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Ho-yehi-maya-ho', 41, 1, 45896, '', '', '', '2015-08-03 13:29:35', '2015-08-03 13:29:35'),
(20, 2, 'Asset/uploads/movie/15052708punte_parade.jpg', 'Punte Parade', '<p>&nbsp;&ldquo;Punte Parade&rdquo; is a story about a boy, a Punte for his mother, and &lsquo;Hari Lathhak&rsquo;, &lsquo;Mandha Buddhi&rsquo; for his father, a boy who has just reached his teenage. As most teenagers, he wants to get noticed, loved and be famous, but never gets anything or at least he thinks so.</p>\r\n<p style="margin: 0in 0in 23.55pt; line-height: 18.55pt; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>', '134178505', '', 'premium', '', 'Punte Parade', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Punte-Parade', 62, 1, 45932, '', '', '', '2015-07-22 08:28:25', '2015-07-22 08:28:25'),
(21, 2, 'Asset/uploads/movie/15052708dhun-poster.jpg', 'Dhoon', '<p>Tara Sitra Production Presnets! "DHOON"<br />Producers: Malati Shah, Shushil Shah, Binod Bhatta, Madhusudhan Dhakal, Shreya Dulal.<br />Cast: Jiwan Luitel, Neeta Dhungana, Mukesh Dhakal, Shreya Dulal, Uma Baby.<br />Music Arranger: Dhiraj Shrestha<br />Story/Direction: Krishna Paudel<br />Choreography: Pradip Shrestha.<br />Camera: Birendra Bhatta.<br />Movie: Dhoon</p>', '135320622', '', 'premium', '', 'Dhoon', 'Jiwan Luitel, Neeta Dhungana, Mukesh Dhakal, Shreya Dulal, Uma Baby', 'Krishna Paudel', 'Lucky Sitaula', 'Nepali', 'English', 'Dhoon', 36, 1, 45896, '', '', '', '2015-08-04 05:52:50', '2015-08-04 05:52:50'),
(22, 3, 'Asset/uploads/movie/15052708tathastu_movie_poster.jpg', 'Tathastu', '<p>Nepali film &lsquo;Tathastu&rsquo; is about Basanti who is abused by her husband. The film about domestic violence and marital rape is a story of a typical Nepali woman abused by her husband. Basanti&rsquo;s husband, Kamal, is an influential political figure. Kamal sexually abuses Basanti before marriage and it continues after the marriage. When things become intolerable, Basanti fakes death and runs away to her village.</p>\r\n<p>When Kamal knew that Basanti is alive, he brings her back. The movie &lsquo;Tathastu&rsquo; is about the fight of Bananti against her abusing husband.</p>', '134177544', '', 'premium', '', 'Tathasthu', 'Rekha Thapa, Kishor Khatiwada, Subash Thapa etc.', 'Shabir Shrestha', 'Unknown', 'Nepali', 'English', 'Tathastu', 38, 1, 45896, '', '', '', '2015-08-03 13:35:29', '2015-08-03 13:35:29'),
(24, 2, 'Asset/uploads/movie/15052708November-Rain-Poster.jpg', 'November Rain', '<p>Descrption of the movie goes here.</p>', '135235082', '', 'premium', '', 'Nepali', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'November-Rain', 39, 1, 45896, '', '', '', '2015-08-03 10:55:06', '2015-08-03 10:55:06'),
(25, 2, 'Asset/uploads/movie/15052708parai.jpg', 'Parai', '<p>Description goes here</p>', '134177553', '', 'premium', '', 'Nepali, Parai', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Parai', 32, 1, 45896, '', '', '<p>Parai Nepali Movie, Nepali Cinema Parai, Nepali Love story Movie, Jharana Thapa Latest Movie, Jharana Thapa Movie</p>', '2015-08-17 08:55:25', '2015-08-17 08:55:25'),
(26, 3, 'Asset/uploads/movie/15052708birkhe_lai_chinchas.jpg', 'Birkhelai Chinchas', '<p>Birkhey lai Chinchhas tells the story of an ordinary man thrust into extraordinary circumstances, taking him on a journey to discover the hero within himself. Birkhey, a happy-go-lucky simpleton from a Nepali mountain village, is sent off by his villagers to find fame and fortune in the big city. But the city is cruel to him and robs him of his innocence, forcing him to make his way in life through guile and treachery. Can a common man face a hostile world and keep his innocence intact? Can an ordinary person rise to become a hero? Birkhey lai Chinchhas is a truly Nepali story with a big heart full of innocence, humor and courage.</p>', '135224613', '', 'premium', '', 'Nepali', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Birkhelai-Chinchas', 58, 1, 45896, '', '', '', '2015-08-03 10:09:38', '2015-08-03 10:09:38'),
(27, 3, 'Asset/uploads/movie/15052708mashan.jpg', 'Mashaan', '<p>The actor and director Nir Shah has started making an art movie, based on the story of late Gopalprasad Rimal&rsquo;s story "<em>Masan</em>". Gopal Prasad is a noted poet and playwright who ushered in a new era in Nepali literature. The drama, <em>Masan</em> is also taught as one of the electives in schools in Nepal and India.</p>', '135247311', '', 'premium', '', 'Mashaan,Art Movie', 'Keki Adhikari, Neeta Dhungana, Raj Ballav Koirala', 'Nir Shah', 'Unknown', 'Nepali', 'English', 'Mashaan', 66, 1, 45896, '', '', '', '2015-08-03 13:29:57', '2015-08-03 13:29:57'),
(28, 2, 'Asset/uploads/movie/15052708seto-bagh-poster-3.jpg', 'Seto Bagh', '<p style="text-align: justify;">Produced jointly by Mahashakti Multi Media and Nir Cinema, &lsquo;Seto Bagh&rsquo; movie and a TV serial on the same story was shot at the same time. The book by the same title was written by by Diamond Shumsher JB Rana is one of the most read literary books in Nepal. cinematography of Sanjay Lama, costume of Nira Rana and the script of Nir Shaha.The picture is directed ny legendary actor and director Nir Shah and casts BS Rana, Shyam Rai, Rabi Giri, Pawan Mainali and Rajaram Poudyal in the lead role.<span style="color: #444444; font-family: ''Source Sans Pro'', sans-serif; font-size: 19px; line-height: 27.2000007629395px;">&nbsp;</span></p>', '134178551', '', 'premium', '', 'Nepali, Novel, Seto Bagh', 'B.S. Rana (As Jung Bahadur Rana), Raja Ram Poudel, Pawan Mainali, Rabi Giri, Shyam Rai', 'Nir Shah', 'Sambhujit Baskota', 'Nepali', 'English', 'Seto-Bagh', 53, 1, 45896, '', '', '<p>Nepali movie seto bag, latest nepali movie, politics related movie, nepali cinema latest</p>', '2015-08-18 06:19:16', '2015-08-18 06:19:16'),
(29, 2, 'Asset/uploads/movie/15052708biteka-pal-poster2.jpg', 'Biteka Pal', '<p>The movie is made on the triangular love story of three characters Numa (Keki Adhikari), Abhi (Abinash Gurung) and Rayan (Baboo Bogati). All three characters are not happy with each other and the movie is the clash between these three characters.</p>', '135247314', '', 'premium', '', 'Biteka Pal,love story ', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Biteka-Pal', 39, 1, 45896, '', '', '', '2015-08-03 13:30:41', '2015-08-03 13:30:41'),
(30, 3, 'Asset/uploads/movie/15052709jangeer.jpg', 'Zanzeer', '<p>Jangeerr Description Goes Here</p>', '135247307', '', 'premium', '', 'Jangeer', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Zanzeer', 38, 1, 45896, '', '', '', '2015-08-03 13:29:11', '2015-08-03 13:29:11'),
(31, 2, 'Asset/uploads/movie/15052709urbashi.jpg', 'Urbashi', '<p>Urbashi Nepali movie</p>', '135247312', '', 'premium', '', 'Urbashi', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Urbashi', 50, 1, 45896, '', '', '', '2015-08-03 13:30:17', '2015-08-03 13:30:17'),
(32, 3, 'Asset/uploads/movie/15052709timro-kasam.jpg', 'Timro Kasam', '<p>Timro Kasam Nepali Movie Poster</p>', '135247319', '', 'premium', '2', 'Timro Kasam', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal.', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Timro-Kasam', 28, 1, 45896, '', '', '', '2015-08-03 13:32:58', '2015-08-03 13:32:58'),
(33, 3, 'Asset/uploads/movie/15052709veer-poster-rekha.jpg', 'Veer', '<p>Rambhai Oli&rsquo;s presentation Nepali film &lsquo;Veer&rsquo; features actress Rekha Thapa with singer and actor Yash Kumar. The action movie features the editing of Milan Shrestha, cinematography of Saurav Lama, action of Dev Maharjan, dance of Kamal Rai / Govinda Rai / Shankar BC / Kabiraj Gahatraj and produced by Prabin Shah.</p>', '135320621', '', 'free', '', 'Veer', 'Jharana Thapa, Suman Singh', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Veer', 100, 1, 45896, '', '', '<p>Nepali movie Veer, Yash Kumar Movie, Rekha Thapa Movie, Bir, Vir&nbsp;</p>', '2015-08-17 08:43:03', '2015-08-17 08:43:03'),
(34, 2, 'Asset/uploads/movie/15053107bhai-haalcha-ni.jpg', 'Vai Haalcha Ni', '<p>It seems like another hot movie in the flood of movies seeking for an &lsquo;adult&rsquo; certificate from the censor board. Hot actress Sonia KC (Soniya KC) is featured in hot dress.</p>', '133032771', '', 'premium', '', 'Bhai Haalcha ni ', 'Suman Singh, Sonia KC, Garima Pant, Bishal Pokharel, Sunil Thapa etc.', 'MS Agaraj', 'Yadav / Govind', 'Nepali', 'English', 'Vai-Haalcha-Ni', 28, 0, 45896, '', '', '', '2015-07-10 11:01:53', '2015-07-10 11:01:53'),
(35, 3, 'Asset/uploads/movie/15053107jaljala.jpg', 'Jaljala', '<p>Nepali movie Jaljala is a&nbsp;movie made on the story of Maoist civil war in Nepal. The movie features Rekha Thapa, Aayush Rijal, Kamal Krishna, Kishor Khatiwada, and late Gopal Bhutani&nbsp;in main roles.</p>\r\n<p>&lsquo;Jaljala&rsquo; features actress Rekha Thapa as a Maoist combatant and it will try to justify the Maoist revolution. The mo<br />vie directed by Shivaji Lamichane also feature the political problem in Nepal because of foreign pressure.</p>\r\n<p>&nbsp;</p>\r\n<p>The movie received &lsquo;A&rsquo; certificate because of violence in the movie.</p>', '35546493', '35546493', 'premium', '2', 'Nepali, War, Civil War,Jaljala', 'Rekha Thapa', 'Shivaji Lamichaane', 'Nanda Kishore poon', 'Nepali', 'English', 'Jaljala', 28, 1, 45896, '', '', '', '2015-08-25 07:01:18', '2015-08-25 07:01:18'),
(36, 2, 'Asset/uploads/movie/15053107rang.jpg', 'Rang', '<blockquote class="tr_bq">Life is measured in colors, golden glow of happiness,&nbsp;bright red of passion and love, dull grey of strife and&nbsp;anger. Darkest deepest, Blackest colors of revenge overshadows all the other colors of life.</blockquote>\r\n<blockquote class="tr_bq">This is the main theme of this new nepali movie. This movie ends in some tragedy and the hero takes revenge with the culprit.</blockquote>\r\n<p style="text-align: center;">&nbsp;</p>', '134178553', '', 'premium', '2', 'Rang, Nepali Movie', 'Jharana Thapa, Suman Singh, Sushil Pokharel, R. L. Maharjan, Shujit Ruchal', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Rang', 37, 1, 45896, '', '', '', '2015-08-25 06:56:32', '2015-08-25 06:56:32'),
(37, 2, 'Asset/uploads/movie/15053107so-simple.jpg', 'So Simple', '<p>Tender love of young boy and girl which is very simple.</p>', '134178549', '', 'premium', '', 'So Simple', 'Bhawana Regmi, Pramod Bhardwaaj, Raj Ballav Koirala, Ashok Sharma, Sweta Bhattarai, Sushil Pokhrel, Nirmal Sharma, Rashmi Bhatta, Binod Bhattarai, Shishir Bhandari', 'Shiva Regmi', 'Tara Prakash Limbu', 'Nepali', 'English', 'So-Simple', 59, 1, 45932, '', '', '', '2015-07-22 08:30:09', '2015-07-22 08:30:09'),
(38, 3, 'Asset/uploads/movie/15053107dashgaja.jpg', 'Dashgaja', '<p>Dasgaja covers issues of border disputes with India, citizenship, and other patriotic issues. The movie was produced by Nawal Khadka and features Nawal, Rajesh Hamal, and Nikhil Upreti in leading role. Dashgaja, literally means &lsquo;No Man&rsquo;s Land,&rsquo;&nbsp; was directed by Daya Ram Dahal.<br /><br />Banned by the censor board of Nepal, this movie was a hype in the question of national negligence shown by our national leaders. If you really want to know what was nepal like? or how much pain one has to face just because they reside near the indian border, this movie is the right stop.</p>', '135320620', '', 'premium', '', 'DashGaja', 'Nawal Khadka, Rajesh Hamal, Nikhil Upreti', 'Daya Ram Dahal', 'Dinesh Sunam', 'Nepali', 'English', 'Dashgaja', 44, 1, 45896, '', '', '', '2015-08-04 06:00:54', '2015-08-04 06:00:54'),
(39, 2, 'Asset/uploads/movie/15053108just-for-you.jpg', 'Just For You', '<p>Just for you is a Nepali movie casting&nbsp;Rajesh Hamal, Badri Pudasaini and other actors. It is directed by Raju Aryal and Dinesh Poudel.</p>', '35546493', '35546493', 'premium', '2', '2014', 'Rajesh Hamal, Badri Pudasaini', 'Raju Aryal, Dinesh Poudel', 'Dinesh Sunam', 'Nepali', 'English', 'Just-For-You', 22, 0, 45896, '', '', '', '2015-07-23 09:57:58', '2015-07-23 09:57:58'),
(40, 2, 'Asset/uploads/movie/15053108pahilo-maya-ho-yo-mero.jpg', 'Pahilo Maya ho Mero', '<p>The movie &lsquo;Pahilo Maya Ho Mero&rsquo; is the presentation of RL Maharjan for Regisha Entertainment Pvt. Ltd. Suman Singh and Jharana Thapa are seen in the lead role. the movie is directed by Ramji Lamichhane and scripted by Sushil Pokharel.</p>\r\n<p>&nbsp;</p>', '135247306', '', 'premium', '', 'Pahilo Maya ho mero', 'Jharana Thapa, Suman Singh', 'Ramji Lamichhane', 'Dinesh Sunam', 'Nepali', 'English', 'Pahilo-Maya-ho-Mero', 33, 1, 45896, '', '', '', '2015-08-04 02:26:37', '2015-08-04 02:26:37'),
(41, 2, 'Asset/uploads/movie/15053108aabeg.jpg', 'Aabeg', '<p>Aabeg is an action Nepali movie produced by Jhamak Narayan Ghimire and Dhan Singh KC. Aryan Sigdel, Dhurba Datta, Archana Delal and Pabitra Acharya can be seen in the movie. It is directed by Basant Niroula.</p>', '35546493', '35546493', 'premium', '', 'Aabeg', 'Aryan Sigdel,  Archana Dellala, Dhruba Dutta, Ramesh Budhathoki, Pavitra Acharya', 'Basanta Niraula', 'Dinesh Sunam', 'Nepali', 'English', 'Aabeg', 11, 0, 45896, '', '', '', '2015-07-21 06:29:55', '2015-07-21 06:29:55'),
(42, 2, 'Asset/uploads/movie/15053108maya-gara-la.jpg', 'Maya Gara la', '<p>A movie produced by madan ghimire and cinematography of Sambhu Sapkota.Mukesh Dhakal, Bhupen Chand, Saujanya Subba, Richa Singh Thakuri can be seen as the star cast of the movie.</p>', '135247315', '', 'premium', '', 'Maya Gara La', 'Mukesh Dhakal, Bhupen Chand, Saujanya Subba, Richa Singh Thakuri', 'Madan Ghimire', 'Dinesh Sunam', 'Nepali', 'English', 'Maya-Gara-la', 23, 1, 45896, '', '', '', '2015-08-03 13:31:27', '2015-08-03 13:31:27'),
(43, 2, 'Asset/uploads/movie/15053109divorce.jpg', 'Divorce', '<p>The movie is based on a story line of husband and wife relationship and the society. The movie also imparts the awareness among youths about the consequences of making sex videos.</p>', '135225215', '', 'premium', '', 'Divorce', 'Kishor Shrestha, Avash Agrahari, Binita Ramtel, Saruna Karki, Bina Thapa, Pradeep Dhakal, Sushil Thapa, Krishna Pandey, Karan Tulachan', 'Manish Panta', 'Dinesh Sunam', 'Nepali', 'English', 'Divorce', 35, 1, 45896, '', '', '', '2015-08-03 10:10:39', '2015-08-03 10:10:39'),
(44, 2, 'Asset/uploads/movie/15053109three-lovers.jpg', 'Three Lovers', '<p>Three Lovers is a strange love story directed by Suresh Darpan Pokharel. &nbsp;Its about 3 guys who have different perception about love . Music of the movie is given by Baboo Bogati and Rajan Ishan.</p>', '35546493', '35546493', 'premium', '', 'Three Lovers', 'Priyanka Karki, Niraj Baral, Debu Shrestha', 'Suresh Darpan Pokharel', 'Baboo Bogati & Rajan Ishan', 'Nepali', 'English', 'Three-Lovers', 29, 1, 45896, '', '', '', '2015-07-23 09:47:43', '2015-07-23 09:47:43'),
(45, 2, 'Asset/uploads/movie/15053109cheers.jpg', 'Cheers', '<p>Cheers Nepali movie story is about the Guy who is in loved with a girl. The Cheers Nepali movie is full of comedy, Romance, Love, Action, Tragedy. Overall it''s show the present status of Nepali society. The Movie Cheers has a political view in some context.</p>', '135320619', '', 'premium', '', 'Cheers, Nepali Movie', 'Mahima Silwal, Paudel, Khusboo Shrestha ', 'Paban Gautam', 'Dinesh Sunam', 'Nepali', 'English', 'Cheers', 44, 1, 45896, '', '', '', '2015-08-04 06:08:18', '2015-08-04 06:08:18'),
(46, 3, 'Asset/uploads/movie/15053109kabja.png', 'Kabja', '<p>Kabja is a Nepali movie starring Biraj Bhatta, Suvechha Thapa, Raju Giri , Krishna Bhatta and many actors. Sanjaye Singh is the story writer. Kishori Films has launched this movie in the direction of Kedar Parajuli.<br /><br /></p>', '135247325', '', 'premium', '', 'Kabza, Nepali, Movie, Biraj Bhatta', 'Biraj Bhatta, Suvechha Thapa, Raju Giri, Krishna Bhatta', 'Kedar Parajuli', 'Unknown', 'Nepali', 'English', 'Kabja', 46, 1, 45896, '', '', '', '2015-08-03 13:33:42', '2015-08-03 13:33:42'),
(47, 2, 'Asset/uploads/movie/15053109anautho-prem-katha.jpg', 'Anautho Prem Katha', '<p>Anautho Prem Katha is an unique love story produced by Pushpa Raj Shahi and Sharmila Shahi. Kanchan Shahi is the director of the movie. Suresh Adhikari has contributed his music to the movie.</p>\r\n<p>&nbsp;</p>', '135223842', '', 'premium', '', 'Nepali Movie, Anautho Prem Katha', 'Sushma Adhikari, Santosh Oli, Kanchan Shahi', 'Kanchan Shahi', 'Suresh Adhikari', 'Nepali', 'English', 'Anautho-Prem-Katha', 37, 1, 45896, '', '', '', '2015-08-03 10:08:25', '2015-08-03 10:08:25'),
(48, 3, 'Asset/uploads/movie/15060107naike.jpg', 'Naike', '<p style="text-align: justify;">The film of a director from Pokhara, Netra Gurung, was shot in Pokhara and the area around the tourist city of Nepal. The movie whose shooting started in the beginning of the year, in January, was released at near the end of the year, on December 5. The movie is made by the director&rsquo;s home production, Black Shadow Production.<br /><br />The movie featured the lover-boy image actor, Aryan Sigdel, in a tough gangster role and featured a number of new actors including the leading actress Surbina Karki. The initial reports in theater however is not that positive for the movie.<br /><br />&nbsp; &nbsp;&nbsp;</p>', '135233739', '', 'premium', '', 'Naike, Nepali Movie, 2014, Aryan Sigdel', 'Aryan Sigdel, Surbina Karki, Rem BK, Bodhraj Regmi, Sarika KC', 'Netra Gurung', 'Tara Prakash Limbu, Yogesh Kaji BK, Raman Gayak', 'Nepali', 'English', 'Naike', 39, 1, 45896, '', '', '', '2015-08-03 10:54:38', '2015-08-03 10:54:38'),
(49, 3, 'Asset/uploads/movie/15060108dhoom.jpg', 'Dhoom', '<p>The story, dialogue, and script of the movie is written by Jaya Kishan Basnet.</p>', '35546493', '35546493', 'premium', '', 'Nepali Movie Dhoom', 'Jaya Kishan Basnet, Biraj Bhatt, Ramit Dhungana, Arjun Karki, Suraj RD, Rajesh Dhungana, Rejina Upreti, Sanjita Gurung, Jenny Kunwar, Shovit Basnet, Shanti Rana etc.', 'Shovit Basnet', 'Unknown', 'Nepali', 'English', 'Dhoom', 27, 0, 45917, '', '', '', '2015-07-10 08:25:35', '2015-07-10 08:25:35'),
(50, 3, 'Asset/uploads/movie/15060108sambodhan.jpg', 'Sambodhan', '<p>&lsquo;Sambodhan&rsquo; is made on the issues of struggle of general public against the government and corruption. For the promotion of the movie the actor Binaya Bhatta has revealed some of the aspects of his co-artist Namrata Shrestha.</p>\r\n<p>&lsquo;Sambodhan&rsquo; is the third film of Hemraj BC and he is expecting it to make a hat-trick of successful films for him.</p>\r\n<p>&lsquo;Sambodhan&rsquo; marks the debut of student politician and the president of Nepal Student Union, Tri Chandra College, Binaya Bhatta. Other actors include Prasant Tamrakar and Suleman Shankar (Iku). &lsquo;Sambodhan&rsquo; is a presentation of Saroj Neupane for Red Rose Entertainment and Blue Throat Films. The film features music by Jayan, script of the director Hemraj BC, produced by Saroj Neupane and Bishnu Lama.</p>\r\n<p>&nbsp;</p>', '135239005', '', 'premium', '', 'Sambodhan,Nepali Movie', 'Dahayang Rai, Namrata Shrestha, Binaya Bhatta, B.S. Rana', 'Hemraj B.C.', 'Jayanj', 'Nepali', 'English', 'Sambodhan', 74, 1, 45896, '', '', '', '2015-08-03 11:09:17', '2015-08-03 11:09:17'),
(51, 3, 'Asset/uploads/movie/15060108chesko.jpg', 'Chesko', '<p>New Nepali Movie CHESKO is directed by Suresh Thapa.</p>', '135224827', '', 'premium', '', 'Chesko, Nepal Movie', 'Unknown', 'Suresh Thapa', 'Unknown', 'Nepali', 'English', 'Chesko', 57, 1, 45896, '', '', '', '2015-08-03 10:10:11', '2015-08-03 10:10:11'),
(52, 3, 'Asset/uploads/movie/15060507mission-four-twenty.jpg', 'Mission Four Twenty', '<p>"Mission 4 Twenty" is based on social issue basically of violence against women. It is directed by Daya Ram Dahal.<br />The movie by a successful and experienced director Dayaram Dahal,&nbsp;&lsquo;Mission 4 Twenty&rsquo; is produced by a journalist Kumar Bista. The director Dahal and Madil Shrestha are the co-producer of the movie. The movie features Suman Singh, Mahima Silwal, Sunil Thapa, Rajaram Poudel, Rabindra Khadka, Nirmal Sharma, Nirmal Senchuri, Krishti Poudel etc. in main roles. The movie featuring a story of struggling youths features action of Roshan Shrestha, cinematography of Shyam Rayamajhi.</p>', '135231577', '', 'premium', '2', 'Four Twenty', 'Suman Singh, Nirmal Century, Kristi Poudel, Sunil Thapa, Shyam Rai, Rajaram Poudyal, Rabindra Khadka, Shekhar Khanal, Dayaram Dahal, Uttam K.C.', 'Daya Ram Dahal', 'Unknown', 'Nepali', 'English', 'Mission-Four-Twenty', 64, 1, 45896, '', '', '', '2015-08-25 06:28:15', '2015-08-25 06:28:15'),
(53, 2, 'Asset/uploads/movie/15060507ajhai-pani.jpg', 'Ajhai Pani', '<p class="MsoNoSpacing">Ajhai Pani is a romantic socio-drama centered around the relationships, social values of Nepalese Society. ''Ajhai Pani is a cute movie about dramatic romance between Yunisha and Kushal. The movie casts Sudarshan Thapa in lead role as cute and handsome Kushal, Puza Sharma as Yunisha and Surakshya Pant as Shila. It also features Mithila Sharma and Ramsharan Pathak as grandparents of Kushal and Bijay Lama as Yunisha&rsquo;s Mama.&nbsp;</p>\r\n<p class="MsoNoSpacing">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNoSpacing">&nbsp;</p>', '135223814', '', 'premium', '', 'Ajhai Pani', 'Sudarshan Thapa, Puja Sharma, Surakshya Panta, Pushpa Khadka, Bishal Dhungana, Bipana Dhamal, Pramila Khanal, Vijay Lama, Bishwa Basnet, Mithila Sharma', 'Alok Nembang', 'Unknown', 'Nepali', 'English', 'Ajhai-Pani', 51, 1, 45896, '', '', '', '2015-08-03 10:07:19', '2015-08-03 10:07:19'),
(54, 1, 'Asset/uploads/movie/15080310paap.jpg', 'Paap', '<p>Nepali Movie &ldquo;Paap&rdquo; is a presentation of diretory NR Ghimire. The movie is produced by Bikash Sharma Gautam, Ram Prasad Sharma, Chandrama Parajuli and Laxman Sapkota under the banner of Bindeshori Cine Arts Pvt Ltd. The movie features Himal Rijal, Sushma Karki, Usha Rai, Sovit Basyal.<br /><br /></p>\r\n<div id="stcpDiv" style="position: absolute; top: -1999px; left: -1988px;">\r\n<p align="justify">Nepali Movie &ldquo;Paap&rdquo; is a presentation of diretory NR Ghimire which is scheduled to be released on 3rd Ashoj this year. The movie is produced by Bikash Sharma Gautam, Ram Prasad Sharma, Chandrama Parajuli and Laxman Sapkota under the banner of Bindeshori Cine Arts Pvt Ltd. The movie features Himal Rijal, Sushma Karki, Usha Rai, Sovit Basyal etc.</p>\r\n<p><strong>Movie Details :</strong><br /> Nepali Movie : Paap<br /> Director : NR Ghimire<br /> Producer : Bikash Sharma<br /> Casts : Himal Rijal, Sushma Karki, Usha Rai, Sovit Basyal etc.<br /> Cinematography : Pawan Gautam<br /> Edit : Arjun GC<br /> Action : Roshan Shrestha<br /> Release Date : Not Fixed Yet<br /> A Presentation of Bindeshori Cine Arts.</p>\r\n- See more at: http://www.onlynepali.net/nepali-movie-paap-trailer-and-videos/#sthash.e5CjtvVy.dpuf</div>', '135236268', '', 'premium', '', 'Paap', 'Himal Rijal, Usha Rai, Sovit Basyal etc.', 'Jivan Rasaili', 'Unknown', 'Nepali', 'English', 'Paap', 58, 1, 45896, '', '', '', '2015-08-03 11:08:35', '2015-08-03 11:08:35'),
(55, 2, 'Asset/uploads/movie/15060508paathshala.jpg', 'Paathshala', '<p>The movie directed by Rajbahadur Sane and produced by Arun Khadka and Keshav Ghimire is released in theaters in Kathmandu. The movie was previously released in theaters out of Kathmandu. &lsquo;Pathshala&rsquo; features the music of Bikash Chaudhary, cinematography of Machhya narayan Shrestha, choreography of Bibek Thapa and action of Briendra Sony. The movie features Birendra Yadav, Raj Bautam, Sangita BC, Menaka Giri, Raj Gautam etc. in main roles.</p>', '135320623', '', 'premium', '', 'Paathshala', 'Birendra Yadav, Raj Gautam, Sangita B.C., Menaka Giri', 'Rajbahadur Sane', 'Bikash Chaudhary', 'Nepali', 'English', 'Paathshala', 113, 1, 45896, '', '', '', '2015-08-04 06:06:30', '2015-08-04 06:06:30');
INSERT INTO `ramro_movie` (`id`, `cat_id`, `image`, `title`, `details`, `trailer_link`, `full_link`, `movie_type`, `price`, `movie_tag`, `cast`, `director`, `music`, `language`, `subtitle`, `url`, `viewed`, `status`, `user_id`, `meta_author`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(56, 2, 'Asset/uploads/movie/15060508rang-baijani.jpg', 'Rang Baijani', '<p>Rang Baijani&nbsp;has two different personalities of women; one who is polite, caring, and well-mannered typical Nepali village girl who is always behind his husband in any situation. Another is modern, seductive, open minded woman who follows her heart and would say anything to get his man.</p>\r\n<p>After the death of father, Rama (Garima Panta) stays at house of her father&rsquo;s best friend who had a son, Binay(Dikpal Karki). Binay&rsquo;s father thinks Rama is suitable for his son and asks Binay to marry her. But Binay reveals that he has a girlfriend, Devina (Sumnima Ghimire) who lives with her &lsquo;fufu&rsquo;. This triggers heart attack to his father. To make her father happy, he marries Rama. When Devina knows about Binay being married, she leaves the city. After three year, Binay finds Devina at hospital. Knowing that Devina has TB, and there is nobody to take care of her, Binay takes Devina to his house. And the drama begins.</p>', '132917057', '', 'premium', '', 'Rang Baijani', 'Sumina Ghimire, Garima Panta, Dikpal Karki, etc.', 'Sambhu Pradhan', 'Deep Pradhan', 'Nepali', 'English', 'Rang-Baijani', 128, 0, 45896, '', '', '', '2015-07-17 08:13:55', '2015-07-17 08:13:55'),
(57, 3, 'Asset/uploads/movie/15071212gunda.jpg', 'Gunda', '<p>A presentation of SS Film Factory and SK Films, Nepali movie &lsquo;Gunda&rsquo;has a lead role of Biraj Bhatta and Anjani Singh. The movie is a dubbed from Bhojpuri movie. The movie is a presentation of Suraj Khadka. The movie is produced by Shrawan Kumar H.</p>', '135247317', '', 'premium', '2', 'Biraj Bhatta, Nepali Movie, Gunda', 'Biraj Bhatta, Anjani Singh, Happy Aryal, Baleshwor Singh', 'Parag Patel, Liladhar Bhattrai ', 'Shankar Adhikari ''Ghayal''', 'Nepali', 'NA', 'Gunda', 267, 1, 45896, '', '', '', '2015-08-25 06:14:02', '2015-08-25 06:14:02'),
(58, 2, 'Asset/uploads/movie/15062608khudkilo.jpg', 'Khudkilo', '<p>Khudkilo is a New Nepali Movie Produced by Sunil Dhakal and Bal Bahadur Shrestha. Janak Khakurel has directed the movie and Sekhar Gawali is the writer of the movie.The movie features cinematography of Saurav Lama, music of Madan Gopal, choreography of Pradeep Shrestha, action of Sanu Kaji Maharjan, editing of Roshan Lamichhane, and made on the story of Sekhar Gyawali. The movie is a presentation of Silk Route Multipurpose.</p>', '134177551', '', 'premium', '2', 'Khudkilo, Nepali Movie, Sushma Karki', 'icha Singh Thakuri, Kiran Laukol, Suraj Singh Thakuri, Shusma Karki.', 'Janak Khakurel', 'Madan Gopal', 'Nepali', 'NA', 'Khudkilo', 254, 1, 45896, '', '', '', '2015-08-25 06:03:52', '2015-08-25 06:03:52'),
(59, 3, 'Asset/uploads/movie/15062609ak47.jpg', 'AK 47', '<p>&lsquo;AK 47&prime; is directed by Brilliant Bista. The movie made by Bishal Films banner features Bishal Bista, Milan Sapkota, Bini Khatiwada, Sushil Pokharel, Deepak Chhetri, Dilip Thakuri etc. in main roles. This movie was also shot three years ago.The movie &lsquo;AK 47&rsquo; is a presentation of Bishal Films. The movie features fight by Dev Maharjan, choreograaphy by Janak Karki, cinematography by Ratna Karki / YP Ghimrie /Niraj Kandel, music by Alok Shree / Dr. Aavash Laav, script /dialogue by Brilliant Bista. The movie is produced by Bishal Bista.</p>', '135243931', '', 'premium', '2', 'AK47, Nepali, Action Movie', 'Bishal Bista, Milan Sapkota, Bini Khatiwada, Sushil Pokharel, Deepak Chhetri, Dilip Thakuri etc.', 'Brilliant Bista', 'Unknown', 'Nepali', 'NA', 'AK-47', 284, 1, 45896, '', '', '', '2015-08-25 05:55:49', '2015-08-25 05:55:49'),
(60, 2, 'Asset/uploads/movie/15080310escape_poster.jpg', 'Escape', '<p>Escape is a story about a mistake committed in the past that hunts an individual irrespective to his present achievements. Escape is not just an awareness to children about effects of drug abuse but it also aims to Project that phase where most of our efforts fail. The phase where these addicts release their mistake and want to rehabilitate them self. This society which look at them and treat in a manner as if they are some kind of criminal with an incurable disease. The children, already physically and mentally weakened, instead of getting support and assurance and love, have to face humiliation and abandonment which is more devastating. I don''t know what is more tragic and painful than giving a severe punished for the mistake in which they had nothing to do except suffer because it is the same materialistic society which incubate drug Mafia who ultimately target our innocent and vulnerable children whose parents were careless. The effort to rehabilitate results in detention and isolation which persuade/compel them to join the only circle that accepts them, drug addicts. Besides current and post effects, the movie intends to projects the real cause behind this growing network of drug Mafia which is equally important and unfortunately not thought about seriously.</p>', '135225721', '', 'premium', '', 'Escape, Nepali Movie', 'Pradeep Khadka, Reema Bishwokarma, Kamal Shrestha, Mukta Babu Acharya, Nischal Kharel, Aarav Ghimire', 'Ananta Ghimire', 'Alish Karki', 'Nepali', 'NA', 'Escape', 27, 1, 45896, '', '', '', '2015-08-03 10:16:33', '2015-08-03 10:16:33'),
(61, 1, 'Asset/uploads/movie/15082405honeymoonnight.jpg', 'Honeymoon Night', '<p>Honeymoon Night Synopsis goes here</p>', '135226005', '', 'premium', '2', 'Honeymoon Night,Hot Nepali Movie', 'Binita Ghimire, Naresh B.K., Sumitra Shrestha, Shambhu Ghimire', 'Laxman Sunar', 'Bharat Tamra', 'Nepali', 'NA', 'Honeymoon-Night', 60, 1, 45896, '', '', '<p>Honeymoon Night Movie feat. Binita Ghimire, Naresh B.K., Sumitra Shrestha, Shambhu Ghimire, Synopsis goes here.</p>', '2015-08-24 05:48:14', '2015-08-24 05:48:14'),
(62, 2, 'Asset/uploads/movie/15080310love_sab_poster.jpg', 'Luv Sab', '<p>Nepali movie &ldquo;Luv Sab&rdquo; featuring Samyam Puri, Salon Basnet and Karishma Shrestha seems to be based on triangular love story.</p>', '135230524', '', 'premium', '', 'Luv Sab, Nepali Romantic Comedy', 'Samyam Puri, Krishana Shrestha, Salon Basnet, Shishir Bhandari', 'Dev Kumar Shrestha', 'Unknown', 'Nepali', 'NA', 'Luv-Sab', 48, 1, 45896, '', '', '', '2015-08-03 10:41:04', '2015-08-03 10:41:04'),
(63, 2, 'Asset/uploads/movie/15080310my-promise_poster.jpg', 'My Promise', '<p>In a dream of better life, Shital migrates to Singapore for work. She is forced to have sex with a agent, Yogi to get a job. So, she runs away from there to protect her dignity. Dibya and Naresh are two army officials, who find Shital in helpless situation and promise to help her. Together they get Yogi beaten. Yogi takes vow to take revenge and make life difficult for those army officials.</p>\r\n<p>&nbsp;</p>', '135232808', '', 'premium', '2', 'My Promise', 'Ramesh Chandra Rai, Keki Adhikari, Aakash Babu Khawas (Mahesh)', 'Suraj Subba Nalbo', 'Mahesh Khadka', 'Nepali', 'NA', 'My-Promise', 87, 1, 45896, '', '', '', '2015-08-26 12:07:19', '2015-08-26 12:07:19'),
(64, 3, 'Asset/uploads/movie/15080311shree5_poster.jpg', 'Shree 5 Ambare', '<p>"Shree Panch Ambare" is absolutely a social movie based on different groups of our society. This movie not only means to provide entertainment but also reflects our social culture. In every field, at least one of the man is found as if he is the king. Shree Panch Ambare is the story of a man who wants to be a king of his own.</p>', '135241631', '', 'premium', '', 'Nepali, Comedy,Movie', 'Saugat Malla, Keki Adhikari, Priyanka Karki', 'Dipa Basnet', 'Unknown', 'Nepali', 'NA', 'Shree-5-Ambare', 93, 1, 45896, '', 'movie, nepali, nepali movie, shree,comedy', '<p>shree pach ambare is a comedyÂ nepali movie</p>', '2015-08-17 09:21:28', '2015-08-17 09:21:28'),
(65, 3, 'Asset/uploads/movie/15080312the_trap_poster.jpg', 'The Trap', '<p>New Nepali Movie ''The Trap'' is a direction of Kamal Yonja. Kaman Singh Lama has contributed his music and lyrics in the movie. This movie depicts how people get trapped in their life. The movie is full of twist and turns. Rojan Rakhal has produced the movie.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '135242637', '', 'premium', '2', 'The Trap, Nepali, Movie', 'Rojan Rakhal, Dinesh Sharma ', 'Kamal Yonja', 'Unknown', 'Nepali', 'NA', 'The-Trap', 97, 0, 45896, '', '', '<p>The Trap Nepali Movie, Nepali Cinema The Trap, Nepali Film Trap, Action Movie&nbsp;</p>', '2015-08-26 12:18:24', '2015-08-26 12:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_banner`
--

DROP TABLE IF EXISTS `ramro_movie_banner`;
CREATE TABLE IF NOT EXISTS `ramro_movie_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `details` longtext NOT NULL,
  `movie_type` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Truncate table before insert `ramro_movie_banner`
--

TRUNCATE TABLE `ramro_movie_banner`;
--
-- Dumping data for table `ramro_movie_banner`
--

INSERT INTO `ramro_movie_banner` (`id`, `title`, `details`, `movie_type`, `link`, `image`, `sort_order`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(7, 'Star Nepali Movie', 'A true love , shine always like star', 'Nepali Movie, Comdey,Drama', 'http://hamromovie.com/video-detail/Star/7', 'Asset/uploads/banner/15060305star_top_banner.jpg', 5, 1, 45896, '2015-04-21', '2015-08-21 11:52:56'),
(8, '6 ekan 6', 'Nepali movie â€˜Chha Ekan Chhaâ€™ is a comedy film by the well known comedy actors of television. ', 'Nepali Movie, Comedy', 'http://hamromovie.com/video-detail/6-ekan-6/15', 'Asset/uploads/banner/15091511website_banner2.jpg', 3, 1, 45896, '2015-04-21', '2015-09-15 11:56:17'),
(9, 'Kanchi Matyang Tyang', 'A fun humorous comedy ride', 'Nepali,Comedy,Drama', 'http://hamromovie.com/video-detail/Kanchi-Matyang-Tyang/14', 'Asset/uploads/banner/15071210kanchi_matyang.jpg', 0, 1, 45896, '2015-04-21', '2015-08-26 13:55:48'),
(10, 'Premika', 'story of a lover', 'Nepali, Drama', 'http://hamromovie.com/video-detail/Premika/16', 'Asset/uploads/banner/15071210premika_banner.jpg', 4, 0, 45896, '2015-04-21', '2015-07-12 10:13:43'),
(11, 'Jhelee', 'A story of Betrayal', 'Nepali Movie,Action, Comedy', 'http://hamromovie.com/video-detail/Jhelee/13', 'Asset/uploads/banner/15071210jhelee.jpg', 6, 1, 45896, '2015-04-21', '2015-08-21 11:55:52'),
(12, 'Sourya â€“ Rise of Hero', 'Sourya is a warrior movie. You will love to watch it.', 'Nepali Movie, Action, Thriller', 'http://hamromovie.com/video-detail/Sourya/11', 'Asset/uploads/banner/15060305saurya.jpg', 2, 1, 45896, '2015-04-21', '2015-08-21 11:48:18'),
(13, 'Promoting Nepali Movie Globally', 'Now your Home is your Theater...', '#AllTimeNepaliMovies', 'http://www.hamromovie.com', 'Asset/uploads/banner/15082405website_banner.jpg', 1, 0, 45896, '2015-08-24', '2015-08-26 13:55:31'),
(14, 'A to Z Nepali Movies', 'All at one place @ hamromovie.com', '#AtoZNepaliMovies', 'http://www.hamromovie.com/', 'Asset/uploads/banner/15091512website_banner.jpg', 1, 1, 45896, '2015-08-28', '2015-09-15 12:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_category`
--

DROP TABLE IF EXISTS `ramro_movie_category`;
CREATE TABLE IF NOT EXISTS `ramro_movie_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Truncate table before insert `ramro_movie_category`
--

TRUNCATE TABLE `ramro_movie_category`;
--
-- Dumping data for table `ramro_movie_category`
--

INSERT INTO `ramro_movie_category` (`id`, `title`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Romance', 45896, '2015-02-25 08:19:24', '2015-02-25 09:19:59'),
(2, 'Love', 45896, '2015-02-25 08:24:21', '2015-02-25 09:20:23'),
(3, 'Action', 45896, '2015-02-25 08:24:30', '2015-08-04 11:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_playlist`
--

DROP TABLE IF EXISTS `ramro_movie_playlist`;
CREATE TABLE IF NOT EXISTS `ramro_movie_playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Truncate table before insert `ramro_movie_playlist`
--

TRUNCATE TABLE `ramro_movie_playlist`;
--
-- Dumping data for table `ramro_movie_playlist`
--

INSERT INTO `ramro_movie_playlist` (`id`, `title`, `sort`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Coming Soon', 2, 1, 45896, '2015-03-03 07:12:04', '2015-06-10 06:56:15'),
(3, 'Free Movies', 1, 1, 45896, '2015-04-06 10:26:43', '2015-04-24 11:31:21');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_premium`
--

DROP TABLE IF EXISTS `ramro_movie_premium`;
CREATE TABLE IF NOT EXISTS `ramro_movie_premium` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `status` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Truncate table before insert `ramro_movie_premium`
--

TRUNCATE TABLE `ramro_movie_premium`;
--
-- Dumping data for table `ramro_movie_premium`
--

INSERT INTO `ramro_movie_premium` (`id`, `movie_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 16, 1, 45911, '2015-04-24 08:15:46', '2015-04-24 08:15:46'),
(2, 15, 1, 45911, '2015-04-25 04:54:22', '2015-04-25 04:54:22'),
(3, 54, 1, 45919, '2015-06-05 08:04:18', '2015-06-05 08:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_subscription`
--

DROP TABLE IF EXISTS `ramro_movie_subscription`;
CREATE TABLE IF NOT EXISTS `ramro_movie_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `suscribe_duration` int(11) NOT NULL COMMENT 'In Days',
  `status` int(5) NOT NULL,
  `subscription_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Truncate table before insert `ramro_movie_subscription`
--

TRUNCATE TABLE `ramro_movie_subscription`;
--
-- Dumping data for table `ramro_movie_subscription`
--

INSERT INTO `ramro_movie_subscription` (`id`, `user_id`, `suscribe_duration`, `status`, `subscription_at`, `update_at`) VALUES
(1, 45911, 30, 1, '2015-04-10 10:28:37', '2015-04-09 12:26:37');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_sub_playlist`
--

DROP TABLE IF EXISTS `ramro_movie_sub_playlist`;
CREATE TABLE IF NOT EXISTS `ramro_movie_sub_playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `status` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Truncate table before insert `ramro_movie_sub_playlist`
--

TRUNCATE TABLE `ramro_movie_sub_playlist`;
--
-- Dumping data for table `ramro_movie_sub_playlist`
--

INSERT INTO `ramro_movie_sub_playlist` (`id`, `playlist_id`, `movie_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(14, 2, 11, 1, 45896, '2015-04-07 13:03:31', '2015-04-07 13:03:31'),
(16, 1, 15, 1, 45896, '2015-04-07 13:05:03', '2015-04-07 13:05:03'),
(17, 1, 13, 1, 45896, '2015-04-07 13:05:19', '2015-04-07 13:05:19'),
(18, 2, 16, 1, 45896, '2015-04-10 07:46:00', '2015-04-10 07:46:00'),
(21, 3, 11, 1, 45896, '2015-04-24 11:32:25', '2015-04-24 11:32:25'),
(22, 3, 7, 1, 45896, '2015-04-24 11:32:30', '2015-04-24 11:32:30'),
(23, 3, 13, 1, 45896, '2015-04-24 11:34:06', '2015-04-24 11:34:06'),
(24, 1, 14, 1, 45896, '2015-04-24 11:45:33', '2015-04-24 11:45:33'),
(25, 1, 16, 1, 45896, '2015-04-24 11:45:49', '2015-04-24 11:45:49'),
(26, 1, 11, 1, 45896, '2015-04-24 11:46:11', '2015-04-24 11:46:11'),
(27, 1, 7, 1, 45896, '2015-04-24 11:46:17', '2015-04-24 11:46:17'),
(28, 1, 17, 1, 45896, '2015-05-27 09:40:43', '2015-05-27 09:40:43'),
(29, 1, 18, 1, 45896, '2015-05-27 09:40:50', '2015-05-27 09:40:50'),
(30, 1, 19, 1, 45896, '2015-05-27 09:40:57', '2015-05-27 09:40:57'),
(31, 1, 20, 1, 45896, '2015-05-27 09:41:04', '2015-05-27 09:41:04'),
(32, 1, 21, 1, 45896, '2015-05-27 09:44:35', '2015-05-27 09:44:35'),
(33, 1, 22, 1, 45896, '2015-05-27 09:44:41', '2015-05-27 09:44:41'),
(34, 1, 24, 1, 45896, '2015-05-27 09:44:47', '2015-05-27 09:44:47'),
(35, 1, 25, 1, 45896, '2015-05-27 09:44:53', '2015-05-27 09:44:53'),
(36, 1, 26, 1, 45896, '2015-05-27 09:45:06', '2015-05-27 09:45:06'),
(37, 1, 27, 1, 45896, '2015-05-27 09:45:12', '2015-05-27 09:45:12'),
(38, 1, 28, 1, 45896, '2015-05-27 09:45:45', '2015-05-27 09:45:45'),
(39, 1, 29, 1, 45896, '2015-05-27 09:45:51', '2015-05-27 09:45:51'),
(41, 1, 30, 1, 45896, '2015-05-27 09:46:06', '2015-05-27 09:46:06'),
(42, 1, 31, 1, 45896, '2015-05-27 09:46:13', '2015-05-27 09:46:13'),
(43, 1, 32, 1, 45896, '2015-05-27 09:46:18', '2015-05-27 09:46:18'),
(44, 1, 33, 1, 45896, '2015-05-27 09:46:25', '2015-05-27 09:46:25'),
(45, 1, 34, 1, 45896, '2015-06-03 05:26:20', '2015-06-03 05:26:20'),
(46, 1, 35, 1, 45896, '2015-06-03 05:26:28', '2015-06-03 05:26:28'),
(47, 1, 36, 1, 45896, '2015-06-03 05:26:35', '2015-06-03 05:26:35'),
(48, 1, 37, 1, 45896, '2015-06-03 05:26:42', '2015-06-03 05:26:42'),
(49, 1, 38, 1, 45896, '2015-06-03 05:26:48', '2015-06-03 05:26:48'),
(50, 1, 39, 1, 45896, '2015-06-03 05:26:55', '2015-06-03 05:26:55'),
(52, 1, 56, 1, 45896, '2015-06-22 08:46:00', '2015-06-22 08:46:00'),
(53, 3, 15, 1, 45896, '2015-06-22 08:46:16', '2015-06-22 08:46:16'),
(54, 1, 57, 1, 45896, '2015-06-26 08:44:50', '2015-06-26 08:44:50'),
(56, 1, 59, 1, 45896, '2015-06-26 09:30:26', '2015-06-26 09:30:26'),
(57, 1, 55, 1, 45896, '2015-06-26 09:47:00', '2015-06-26 09:47:00'),
(58, 1, 60, 1, 45896, '2015-08-03 10:17:26', '2015-08-03 10:17:26'),
(59, 1, 61, 1, 45896, '2015-08-03 10:30:06', '2015-08-03 10:30:06'),
(60, 1, 62, 1, 45896, '2015-08-03 10:41:43', '2015-08-03 10:41:43'),
(61, 1, 63, 1, 45896, '2015-08-03 10:54:09', '2015-08-03 10:54:09'),
(62, 1, 64, 1, 45896, '2015-08-03 11:52:49', '2015-08-03 11:52:49'),
(63, 1, 65, 1, 45896, '2015-08-03 12:20:22', '2015-08-03 12:20:22'),
(64, 3, 33, 1, 45896, '2015-08-17 08:56:32', '2015-08-17 08:56:32');

-- --------------------------------------------------------

--
-- Table structure for table `ramro_movie_user`
--

DROP TABLE IF EXISTS `ramro_movie_user`;
CREATE TABLE IF NOT EXISTS `ramro_movie_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(80) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `social_media` varchar(80) NOT NULL,
  `social_id` text NOT NULL,
  `email_verified` int(3) NOT NULL DEFAULT '0',
  `auto_gen_code` varchar(40) NOT NULL,
  `pass` varchar(250) NOT NULL,
  `role` int(5) NOT NULL COMMENT '1 is Top Admin,2 is sub admin and  3 is Customer',
  `status` int(5) NOT NULL DEFAULT '1',
  `active` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45964 ;

--
-- Truncate table before insert `ramro_movie_user`
--

TRUNCATE TABLE `ramro_movie_user`;
--
-- Dumping data for table `ramro_movie_user`
--

INSERT INTO `ramro_movie_user` (`id`, `full_name`, `first_name`, `last_name`, `image`, `email`, `gender`, `social_media`, `social_id`, `email_verified`, `auto_gen_code`, `pass`, `role`, `status`, `active`, `created_at`, `updated_at`) VALUES
(45896, '', 'Ramro', 'Movie', '', 'mero@gmail.com', '', '', '', 0, '', '0192023a7bbd73250516f069df18b500', 1, 1, 0, '2015-02-18 10:37:33', '2015-02-18 15:20:50'),
(45910, '', 'Rex', 'Thapa', '', 'raghu.kataria@gmail.com', '', 'normal', '', 0, '', '0192023a7bbd73250516f069df18b500', 3, 1, 0, '2015-04-03 12:02:55', '2015-04-03 12:02:55'),
(45911, 'Raghu Chd', 'Sudarshan', 'Mahato', '', '', 'male', 'facebook', '448051132010916', 0, '', '', 3, 1, 0, '2015-04-05 06:22:00', '2015-04-05 06:22:00'),
(45912, 'Raghav Cdh', 'Raghu', 'Chaudhary', '', '', 'male', 'facebook', '1085815248101243', 0, '', '', 3, 1, 0, '2015-04-05 10:12:54', '2015-04-05 10:12:54'),
(45913, 'Imam Kashif', 'Kashif', 'Imam', '', '', 'male', 'facebook', '899145663441944', 0, '', '', 3, 1, 0, '2015-04-05 10:48:06', '2015-04-05 10:48:06'),
(45914, 'ANil DhAkal', 'ANil', 'DhAkal', '', '', 'male', 'facebook', '747767552010255', 0, '', '', 3, 1, 0, '2015-04-08 10:40:32', '2015-04-08 10:40:32'),
(45915, 'Shankar Uprety', 'Shankar', 'Uprety', '', '', 'male', 'facebook', '10155378857720048', 0, '', '', 3, 1, 0, '2015-04-09 18:04:07', '2015-04-09 18:04:07'),
(45916, 'Om Gurung', 'Om', 'Gurung', '', '', 'male', 'facebook', '10206532042642588', 0, '', '', 3, 1, 0, '2015-04-15 07:59:19', '2015-04-15 07:59:19'),
(45917, 'Subash Sapkota', 'Subash', 'Sapkota', '', '', 'male', 'facebook', '970458112966651', 0, '', '', 3, 1, 0, '2015-04-17 11:19:57', '2015-04-17 11:19:57'),
(45918, 'ankur bhattarai', 'ankur', 'bhattarai', '', 'bhattaraiankur@gmail.com', '', 'normal', '', 0, '', 'cf1e7d9ade76e056b080794ef4a417bb', 3, 1, 0, '2015-04-20 06:50:54', '2015-04-20 06:50:54'),
(45919, 'Mahesh Bhandari', 'Mahesh', 'Bhandari', '', '', 'male', 'facebook', '999732866706570', 0, '', '', 3, 1, 0, '2015-04-24 06:13:16', '2015-04-24 06:13:16'),
(45920, 'Rex Chaudhary', 'Rex', 'Chaudhary', '', 'raghav_nu@yahoo.com', '', 'normal', '', 0, '', '69e16d5e9c5cfa291bc52dd0732a5cec', 3, 1, 0, '2015-05-03 13:21:26', '2015-05-03 13:21:26'),
(45921, 'Sameer Gurung', 'Sameer', 'Gurung', '', '', 'male', 'facebook', '835488569838306', 0, '', '', 3, 1, 0, '2015-06-03 12:03:22', '2015-06-03 12:03:22'),
(45922, 'Saru Subedi', 'Saru', 'Subedi', '', 'saru.subedi@gmail.com', '', 'normal', '', 0, '', '437da588cb5595fb1a0bd39187fe61b4', 3, 1, 0, '2015-06-07 17:00:12', '2015-06-07 17:00:12'),
(45923, 'Thomas Rosicky', 'Thomas', 'Rosicky', '', '', 'female', 'facebook', '1596489173944482', 0, '', '', 3, 1, 0, '2015-07-02 06:51:29', '2015-07-02 06:51:29'),
(45924, 'Anand Dhital', 'Anand', 'Dhital', '', '', 'male', 'facebook', '10207423658853607', 0, '', '', 3, 1, 0, '2015-07-06 09:25:40', '2015-07-06 09:25:40'),
(45925, 'Nabin Giri', 'Nabin', 'Giri', '', '', 'male', 'facebook', '1864785897080332', 0, '', '', 3, 1, 0, '2015-07-07 08:24:05', '2015-07-07 08:24:05'),
(45926, 'Da Nepliz', 'Da', 'Nepliz', '', '', 'male', 'facebook', '10152941188201669', 0, '', '', 3, 1, 0, '2015-07-12 07:20:20', '2015-07-12 07:20:20'),
(45927, 'Dipak Bastola', 'Dipak', 'Bastola', '', '', 'male', 'facebook', '1168094239874841', 0, '', '', 3, 1, 0, '2015-07-12 11:28:58', '2015-07-12 11:28:58'),
(45928, 'AerOoz Shahi', 'AerOoz', 'Shahi', '', '', 'male', 'facebook', '1080945681934600', 0, '', '', 3, 1, 0, '2015-07-13 11:38:03', '2015-07-13 11:38:03'),
(45929, 'Rajesh Khadka', 'Rajesh', 'Khadka', '', '', 'male', 'facebook', '10205478871466107', 0, '', '', 3, 1, 0, '2015-07-14 04:00:54', '2015-07-14 04:00:54'),
(45930, 'Purna Thapa', 'Purna', 'Thapa', '', '', 'male', 'facebook', '995618127123070', 0, '', '', 3, 1, 0, '2015-07-15 05:03:27', '2015-07-15 05:03:27'),
(45931, 'RevoKed Cp', 'RevoKed', 'Cp', '', '', 'male', 'facebook', '10203566184875219', 0, '', '', 3, 1, 0, '2015-07-18 05:05:24', '2015-07-18 05:05:24'),
(45932, 'Anil Dhakal', 'Anil', 'Dhakal', '', 'anilsag2006@gmail.com', '', 'normal', '', 0, '', 'afbcd29651458ae8ed8456e00d7ea1ea', 3, 1, 0, '2015-07-22 08:25:00', '2015-07-22 08:25:00'),
(45933, 'Aasish Limbu', 'Aasish', 'Limbu', '', '', 'male', 'facebook', '960001797375487', 0, '', '', 3, 1, 0, '2015-08-02 08:06:27', '2015-08-02 08:06:27'),
(45934, 'lalmonm lastname', 'lalmonm', 'lastname', '', 'ruqstar@ymail.com', '', 'normal', '', 0, '', '29c3eea3f305d6b823f562ac4be35217', 3, 1, 0, '2015-08-06 06:30:15', '2015-08-06 06:30:15'),
(45935, 'Sagar Maharjan', 'Sagar', 'Maharjan', '', '', 'male', 'facebook', '745226115605553', 0, '', '', 3, 1, 0, '2015-08-06 10:22:05', '2015-08-06 10:22:05'),
(45936, 'Ashim Lamichhane', 'Ashim', 'Lamichhane', '', 'punchedrock@gmail.com', '', 'normal', '', 0, '', '16bdcbc8fcf5533f54f64ff9c05bbf5e', 3, 1, 0, '2015-08-12 10:01:36', '2015-08-12 10:01:36'),
(45937, 'Da Nepliz', 'Da Nepliz', 'Da Nepliz', '', '', 'male', 'facebook', '10153006683231669', 0, '', '', 3, 1, 0, '2015-08-13 06:02:41', '2015-08-13 06:02:41'),
(45938, 'da Karki', 'da', 'Karki', 'https://lh3.googleusercontent.com/-wjuCbdmQAYA/AAAAAAAAAAI/AAAAAAAACm4/DemIwRbF860/photo.jpg', 'danepliz@gmail.com', 'male', 'google', '109934407389404553279', 0, '', '', 3, 1, 0, '2015-08-14 07:27:26', '2015-08-14 11:48:51'),
(45939, 'ashim lamichhane', 'ashim', 'lamichhane', 'https://lh5.googleusercontent.com/-kfSo30oR4YE/AAAAAAAAAAI/AAAAAAAAAec/7l4Q9Fh325A/photo.jpg', 'punchedrock@gmail.com', 'male', 'google', '108115438870009003482', 0, '', '', 3, 1, 0, '2015-08-14 08:30:22', '2015-08-14 08:30:22'),
(45940, 'Ratna Katuwal', 'Ratna', 'Katuwal', '', 'ratna@gmail.com', '', 'normal', '', 0, '', 'e10adc3949ba59abbe56e057f20f883e', 3, 1, 0, '2015-08-14 12:20:55', '2015-08-14 12:20:55'),
(45941, 'avaya rijal', 'avaya', 'rijal', 'https://lh6.googleusercontent.com/-_E2hWeUtZLw/AAAAAAAAAAI/AAAAAAAAAaQ/BJ9rK58l3w4/photo.jpg', 'avayarijal@gmail.com', 'male', 'google', '109845357471974709782', 0, '', '', 3, 1, 0, '2015-08-14 12:40:11', '2015-08-14 12:40:11'),
(45942, 'Raghav Cdh', 'Raghav', 'Cdh', 'https://lh5.googleusercontent.com/-P0BYA5eN84k/AAAAAAAAAAI/AAAAAAAABoE/TObtNV0XGdo/photo.jpg', 'raghu.kataria@gmail.com', 'male', 'google', '109446495009537506453', 0, '', '', 3, 1, 0, '2015-08-15 09:49:24', '2015-08-15 09:51:05'),
(45943, 'Ratna Katuwal', 'Ratna Katuwal', 'Ratna Katuwal', '', '', 'male', 'facebook', '1110685115625450', 0, '', '', 3, 1, 0, '2015-08-16 08:29:43', '2015-08-16 08:29:43'),
(45944, 'Subash Sapkota', 'Subash Sapkota', 'Subash Sapkota', '', '', 'male', 'facebook', '1031817700164025', 0, '', '', 3, 1, 0, '2015-08-16 10:28:35', '2015-08-16 10:28:35'),
(45945, 'Subash Sapkota', 'Subash', 'Sapkota', 'https://lh5.googleusercontent.com/-LKjAcrb8FuM/AAAAAAAAAAI/AAAAAAAAAE4/FKFZwVTat08/photo.jpg', 'subashsapkota9@gmail.com', 'male', 'google', '106930020412142021216', 0, '', '', 3, 1, 0, '2015-08-16 10:29:08', '2015-08-16 10:29:08'),
(45946, 'Avaya Rzl', 'Avaya Rzl', 'Avaya Rzl', '', '', 'male', 'facebook', '10153512518229598', 0, '', '', 3, 1, 0, '2015-08-16 12:32:58', '2015-08-16 12:32:58'),
(45947, 'Thomas Rosicky', 'Thomas', 'Rosicky', '', 'cashonad69@gmail.com', 'male', 'facebook', '1616631385263594', 0, '', '', 3, 1, 0, '2015-08-17 05:58:00', '2015-08-21 04:52:07'),
(45948, 'Raghu Chd', 'Raghu Chd', 'Raghu Chd', '', '', 'male', 'facebook', '501746069974755', 0, '', '', 3, 1, 0, '2015-08-21 06:29:31', '2015-08-21 06:29:31'),
(45949, 'Amar Grg', 'Amar', 'Grg', 'https://lh6.googleusercontent.com/-Th1QP3sb3-M/AAAAAAAAAAI/AAAAAAAAAoM/tt1-4L7-q54/photo.jpg', 'aswrgrg@gmail.com', 'male', 'google', '108282602597845859852', 0, '', '', 3, 1, 0, '2015-08-21 06:50:38', '2015-08-21 06:50:38'),
(45950, 'Mahesh Bhandari', 'Mahesh Bhandari', 'Mahesh Bhandari', '', '', 'male', 'facebook', '1062778143735375', 0, '', '', 3, 1, 0, '2015-08-21 07:03:56', '2015-08-21 07:03:56'),
(45951, 'Amar Gurung', 'Amar Gurung', 'Amar Gurung', '', '', 'male', 'facebook', '10206864308993717', 0, '', '', 3, 1, 0, '2015-08-21 11:20:30', '2015-08-21 11:20:30'),
(45952, 'Ramdev Baba', 'Ramdev', 'Baba', 'https://lh4.googleusercontent.com/-qCJUEd-GIFY/AAAAAAAAAAI/AAAAAAAAABQ/rjFC4ngllj4/photo.jpg', 'cashonad69@gmail.com', 'male', 'google', '115619446844750491099', 0, '', '', 3, 1, 0, '2015-08-21 11:23:22', '2015-08-21 11:23:22'),
(45953, 'Anil Dhakal', 'Anil', 'Dhakal', 'https://lh6.googleusercontent.com/-baVN0LWlgRo/AAAAAAAAAAI/AAAAAAAAFiM/CNmWSxx28Bo/photo.jpg', 'anilsag2006@gmail.com', 'male', 'google', '106694920831586791869', 0, '', '', 3, 1, 0, '2015-08-21 11:30:05', '2015-08-21 11:30:46'),
(45954, 'Shankar Uprety', 'Shankar Uprety', 'Shankar Uprety', '', '', 'male', 'facebook', '10155912204945048', 0, '', '', 3, 1, 0, '2015-08-21 15:11:01', '2015-08-21 15:11:01'),
(45955, 'Himal Niraula', 'Himal', 'Niraula', 'https://lh3.googleusercontent.com/-agGCfLV0Raw/AAAAAAAAAAI/AAAAAAAABVI/xyukKHSLQF4/photo.jpg', 'niraula.himal@gmail.com', 'male', 'google', '118246516579657255274', 0, '', '', 3, 1, 0, '2015-08-21 15:55:07', '2015-08-21 15:55:07'),
(45956, 'ANil DhAkal', 'ANil DhAkal', 'ANil DhAkal', '', '', 'male', 'facebook', '825096730944003', 0, '', '', 3, 1, 0, '2015-08-24 04:39:38', '2015-08-24 04:39:38'),
(45957, 'raju thapa', 'raju', 'thapa', '', 'rajumagar23@yahoo.com', '', 'normal', '', 0, '', '3354045a397621cd92406f1f98cde292', 3, 1, 0, '2015-08-27 06:33:01', '2015-08-27 06:33:01'),
(45958, 'Chhabi Paudel', 'Chhabi', 'Paudel', '', 'chhabichhabi1@outlook.com', '', 'normal', '', 0, '', 'c70e71514a2e8b49cb821d902a1caeef', 3, 1, 0, '2015-08-27 16:56:37', '2015-08-27 16:56:37'),
(45959, 'Reut Rana', 'Reut Rana', 'Reut Rana', '', '', 'male', 'facebook', '813990442052086', 0, '', '', 3, 1, 0, '2015-08-29 13:08:09', '2015-08-29 13:08:09'),
(45960, 'Sudip Sapkota', 'Sudip Sapkota', 'Sudip Sapkota', '', '', 'male', 'facebook', '1077501408934540', 0, '', '', 3, 1, 0, '2015-08-31 08:53:29', '2015-08-31 08:53:29'),
(45961, 'Ashim Lamichhane', 'Ashim Lamichhane', 'Ashim Lamichhane', '', '', 'male', 'facebook', '10152982016992461', 0, '', '', 3, 1, 0, '2015-09-03 10:14:14', '2015-09-03 10:14:14'),
(45962, 'sibjan chaulagain', 'sibjan', 'chaulagain', 'https://lh5.googleusercontent.com/-wcLTNPZ6Ftg/AAAAAAAAAAI/AAAAAAAAAjw/sm_I6QdwQI4/photo.jpg', 'sibjanc@gmail.com', 'male', 'google', '107119829016226480838', 0, '', '', 3, 1, 0, '2015-09-07 10:25:27', '2015-09-07 10:25:27'),
(45963, 'Sanjay Shrestha', 'Sanjay Shrestha', 'Sanjay Shrestha', '', '', 'male', 'facebook', '10203134448282705', 0, '', '', 3, 1, 0, '2015-09-12 15:33:55', '2015-09-12 15:33:55');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
