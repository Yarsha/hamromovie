<?php

namespace Auth\Controller {

    use App;
    use Auth\Model\Auth as AuthModel;
    use CaptchaMath;
    use Input;
    use Request;
    use Response;
    use Validate;
    use View;

    class Auth
    {
        // INDEX
        public function Login()
        {

            $data = array();
            $input = (object)Input::post();

            if (Request::isPost()) {

                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->email, $displayName = 'Email', $req = TRUE);
                Validate::Str($input->pass, $displayName = 'Password', $req = TRUE, $min = 3, $max = 19);
                Validate::Match($input->inpSum, $input->hidSum, $displayName = 'Captcha');

                if (Validate::IsFine()) {

                    if (AuthModel::LoginCheck($input->email, $input->pass)) {

                        Response::redirect(App::urlFor('dashboard'));

                    } else {

                        $data['errMsg'] = "Invalid Access";
                    }

                }
            }// END POST

            //CAPTCHA DISPLAY
            CaptchaMath::Instance();
            $data['dspNum'] = CaptchaMath::DisplayNum();
            $data['hidSum'] = CaptchaMath::HiddenSum();

            View::display('@Auth/login.twig', $data);
        }

        // LOGOUT
        public function Logout()
        {
            unset($_SESSION);
            $_SESSION = array();
            session_destroy();
            Response::redirect(App::urlFor('login'));
        }

        // FORGOT
        public function Forgot()
        {
            $from = HOST_EMAIL;
            $host = WEBSITE_NAME;

            $data['v'] = Validate::Instance();

            if (Request::isPost() && sizeof(Request::post()) > 2) {

                $input = (object)Input::post();

                $to = $input->email;
                $inpSum = $input->inpSum;
                $hidSum = $input->hidSum;

                Validate::Email($to, 'Email', TRUE);
                Validate::Match($inpSum, $hidSum, 'Captcha');

                if (Validate::isFine()) {

                    $row = DB::dlookup('email, verification_code', 'nrn_user', 'email = ?', array($to));

                    if ($row) {

                        $subject = 'Password Change Request';
                        $message = '<html>
                                        <head>
                                        <title></title>
                                        </head>
                                        <body style="font-family: arial, verdana, sans-serif">
                                            <div style="">
                                            <!--cf9b3d  191970-->
                                                <div style="background:#E64A19;padding:8px;z-index: -1;width:800px;border-radius:5px 5px 0px 0px;margin-bottom:1px;box-shadow: 0 25px 55px 0 rgba(0, 0, 0, 0.21), 0 16px 28px 0 rgba(0, 0, 0, 0.22);"><img src="' . $host . 'Asset/Site/image/kripa_logo.png" height="60px" width="80px"/></div>
                                            </div>
                                            <div style="background:#FFFFFF;border:1px solid #d3d3d3;text-align:center;padding:8px;width:798px;height:300px;border-radius:3px;color:white;">
                                                <table border="0" align="center">
                                                    <tr>
                                                        <td colspan="2" align="center"><span style="color:#FF5722;font-size:17px;">Click the Link to Change Password</span></td>
                                                    </tr>
                                                    <tr><td colspan="2">&nbsp;</td></tr>
                                                    <tr>
                                                        <td colspan="2" ><span style="color:#B6B6B6;">LINK</span> : &nbsp; &nbsp;<span style="color:#B6B6B6;text-decoration:underline;"><a href="' . $host . 'admin/forgot-change-password?e=' . $to . '&s=' . $row['auto_gen_code'] . '">Click Here</a></span></td>
                                                    </tr>
                                                </table>
                                                <div style="clear:both;"></div>
                                                <div style="color:#B6B6B6;margin-top:50px;font-size:15px;">
                                                Copyright (c) 2014 Kripa Unplugged Pvt. Ltd. All Rights Reserved
                                                </div>
                                            </div>
                                        </body>
                                    </html>';

                        $headers = "From: Kripa Unplugged " . strip_tags($from) . "\r\n";
                        $headers .= "Reply-To: " . strip_tags($from) . "\r\n";
                        //$headers .= "CC: susan@example.com\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


                        if (mail($to, $subject, $message, $headers)) {
                            $data['reset'] = true;
                            $data['succMsg'] = " Pass Code sent to : " . $to;
                        } else {
                            $data['errMsg'] = "Error in sending E-mail: " . $to;
                        }
                    } else {
                        $data['error'] = TRUE;
                        $data['errMsg'] = "Invalid Email";
                    }
                }
            }

            //CAPTCHA DISPLAY
            CaptchaMath::Instance();
            $data['dspNum'] = CaptchaMath::DisplayNum();
            $data['hidSum'] = CaptchaMath::HiddenSum();

            View::display('@Auth/forgot.twig', $data);
        }

        // FORGOT CHANGE PASSWORD
        public function ForgotChangePassword()
        {
            $data = array();

            $data['v'] = Validate::Instance();

            $data['v_code'] = preg_replace('/[^0-9a-zA-Z]/', '', Input::params('s'));
            $data['email'] = Input::params('e');

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                // GRAB ALL POST DATA
                $input = (object)Input::post();

                // CHECK USER
                DB::select('id', 'nrn_user', 'email = ? AND verification_code = ?', array($input->e, $input->s));
                $user = DB::fetch_obj();

                if (!$user) {
                    Response::redirect(App::urlFor('login'));
                }

                Validate::Str($input->pass, 'New Password', TRUE, 4, 19);
                Validate::Match($input->pass, $input->pass_retype, 'Retype Password');
                Validate::Match($input->inpSum, $input->hidSum, 'Captcha');

                if (Validate::isFine()) {

                    if (AuthModel::ChangePassword($input->pass_retype, $user->id)) {

                        $data['succMsg'] = "Password changed Successfully You may now Login";

                    } else {
                        $data['errMsg'] = "Invalid Request";
                    }
                }
            }

            //CAPTCHA DISPLAY
            CaptchaMath::Instance();
            $data['dspNum'] = CaptchaMath::DisplayNum();
            $data['hidSum'] = CaptchaMath::HiddenSum();

            View::display('@Auth/forgot-change-password.twig', $data);
        }

        // CHANGE PASSWORD
        public function EditPassword()
        {

            $data = array();

            //CHECK IF LOGGED IN
            AuthModel::IsLogged(array('loggedIn' => true));

            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                $old_pass   = Input::post('old_pass');
                $new_pass1  = Input::post('new_pass1');
                $new_pass2  = Input::post('new_pass2');

                Validate::Str($old_pass, 'Old Password', TRUE);
                Validate::Str($new_pass1, 'New Password', TRUE, 4, 19);
                Validate::Match($new_pass1, $new_pass2, 'Retype Password');

                if (Validate::isFine()) {

                    $user_id    = AuthModel::GetSessionValue('id');
                    $pass       = AuthModel::CheckUserPassword($old_pass, $user_id);

                    if ($pass) {

                        if (AuthModel::ChangePassword($new_pass1, $user_id)) {

                            $data['succMsg'] = "Password changed Successfully";
                        }
                    } else {
                        $data['errMsg'] = "Invalid Old Password";
                    }
                }
            }

            View::display('@Auth/change-password.twig', $data);
        }

        // VIEW USER DETAILS
        public function UserDetail(){

            $data = array();

            //CHECK IF LOGGED IN
            AuthModel::IsLogged(array('loggedIn' => true));
            $user_id    = AuthModel::GetSessionValue('id');

            if(AuthModel::UserDetail($user_id) !== FALSE){
                    $data['row'] = AuthModel::UserDetail($user_id);
            }else{
                $data['errMsg'] = "Error in retrieving Data";
            }

            View::display('@Auth/user-detail.twig', $data);
        }
    }
}