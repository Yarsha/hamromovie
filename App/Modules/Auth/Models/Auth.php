<?php

namespace Auth\Model {

    use App;
    use DB;
    use Session;

    class Auth
    {

        protected static $_table = "ramro_movie_user";

        public static function LoginCheck($email, $pass)
        {
            $row = DB::dlookup('id,first_name,last_name,email,role', self::$_table, 'email = ? and pass = ?', array($email, md5($pass)));

            if ($row) {

                $session = New Session();
                $session->setSession(array(
                    'id' => $row['id'],
                    'fname' => $row['first_name'],
                    'lname' => $row['last_name'],
                    'role' => $row['role'],
                    'country' => $row['country'],
                    'loggedIn' =>true,
                    'email' => $row['email']
                ));

                return TRUE;

            } else {

                return FALSE;
            }
        }

        public static function IsLogged($adminData)
        {
            $session = New Session();

            foreach ($adminData as $key => $val) {
                if ($session->getSession($key) != $val) {
                    App::redirect(App::urlfor('login'));
                }
            }

        }

        public static function GetSessionValue($type)
        {
            $session = New Session();
            return $session->getSession($type);

        }

        public static function SetSessionValue($sessData)
        {
            $session = New Session();
            return $session->setSession($sessData);

        }

        public static function ChangePassword($password, $user_id)
        {

            if (DB::update(self::$_table, array('pass' => md5($password)), 'id = ?', array($user_id))) {

                return true;
            } else {
                return false;
            }
        }

        public static function CheckUserPassword($password, $user_id)
        {
            $row = DB::dlookup('pass', self::$_table, 'pass = ? AND id = ?', array(md5($password), $user_id));
            if ($row) {
                return true;
            } else {
                return false;
            }
        }

        public static function UserDetail($user_id)
        {

            DB::select(array('first_name', 'last_name','image','email'), self::$_table, 'id = ?', array($user_id));
            $row = DB::fetch_assoc();

            if ($row) {
                return $row;

            } else {
                return false;

            }
        }

    }
}