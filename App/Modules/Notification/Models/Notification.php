<?php

namespace Notification\Models {

    use GCM;
    use DB;
    use Encryption;

    class Notification
    {

        protected static $GCM_API_KEY   = 'AIzaSyCe04hMNSHqsFI9bdrm4JenWL9zaFuGPio';
        protected static $APNS_API_KEY  = '';

        public static function CheckTitle($title)
        {

            if (DB::dcount('id','kripa_notification_history', 'title = ?', array($title))) {

                return TRUE;
            } else {

                return FALSE;
            }

        }

        public static function GetAllIds()
        {
            $gcmRegIds = array();
            DB::query('SELECT reg_id FROM  kripa_notification', '', true);
            $result = DB::fetch_assoc_all();

            foreach($result as $key=>$val){

                array_push($gcmRegIds, $val['reg_id']);

            }

            return isset($gcmRegIds) ? $gcmRegIds : FALSE;
        }

        public static function Add($data)
        {
            $id = DB::insert('kripa_notification_history', $data);

            return isset($id) ? $id : FALSE;
        }

        public static function Remove($ids)
        {
            $enc = New Encryption();
            $res = false;
            foreach ($ids as $did) {
                DB::delete('kripa_notification_history', 'id = ?', array($enc->decode($did)));
                $res = True;
            }

            return $res;

        }

        public static function SaveRegId($rawData)
        {

            $id = DB::insert('kripa_notification', $rawData);

            return isset($id) ? $id : FALSE;

        }

        public static function SendToGcm($regIds, $msgNotification)
        {

            if ($response = GCM::PushNotification($regIds, $msgNotification, self::$GCM_API_KEY)) {

                return $response;

            } else {
                return $response;
            }
        }

        public function SendToApns()
        {


        }

    }
}