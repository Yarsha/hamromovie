<?php

namespace Api\v1\Controller;

use Api\v1\Model\Album as AlbumModel;
use App;
use DB;
use Input;
use Response;
use VIew;

class Album
{

    public function Index()
    {
        $max_id   = preg_replace('/[^0-9]/', '', Input::get('max_id'));
        $since_id = preg_replace('/[^0-9]/', '', Input::get('since_id'));

        if (AlbumModel::GetAll($since_id,$max_id)) {

            $data['error'] = false;
            $data['message'] = "Success";
            $data['albumData'] = AlbumModel::GetAll($since_id,$max_id);
            $code = 200;

        } else {

            $data['error'] = true;
            $data['message'] = "Error in Retrieving";
            $code = 200;

        }

        Response::json($code, $data);

    }
}