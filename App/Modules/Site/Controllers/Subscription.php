<?php
/**
 * Created by PhpStorm.
 * User: Bidhee
 * Date: 4/13/2015
 * Time: 3:46 PM
 */

namespace Site\Controller;

use App;
use DB;
use Input;
use Request;
use Response;
use Site\Model\Subscription as SubscriptionModel;
use View;

class Subscription
{

    public function Show()
    {
        $data = [];

        View::display('@Site/subscription/subscription.twig', $data);
    }


    public function SubscriptionMessage(){
        View::display('@Site/subscription/message.twig', []);
    }

}