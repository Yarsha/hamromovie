<?php

/**
 * Created by PhpStorm.
 * User: Bidhee
 * Date: 4/8/2015
 * Time: 11:28 AM
 */

namespace Site\Model;

use DB;
use App;
class Video {

    public static function GetVideoById($url, $video_id) {

        $sql = "
                SELECT

                m.id as mid,
                m.title as title,
                m.image as image,
                m.details as details,
                m.trailer_link  as trailerLink,
                m.price  as price,
                m.movie_type  as movieType,
                m.movie_tag  as movieTag,
                m.cast  as cast,
                m.director  as director,
                m.music  as music,
                m.language  as language,
                m.subtitle  as subtitle,
                m.viewed  as view,
                m.url  as url,
                m.meta_author as meta_author,
                m.meta_keywords as meta_keywords,
                m.meta_description as meta_description

            FROM
                ramro_movie as m

            RIGHT JOIN

                ramro_movie_category as c

            ON
                m.cat_id = c.id

            WHERE
                m.status=1
            AND
                m.url = ?
            AND
                m.id = ?
            ";

        DB::query($sql, [$url, $video_id]);
        $result = DB::fetch_assoc();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function GetVideoByIdOnly($video_id) {

        $sql = "
                SELECT

                m.id as mid,
                m.title as title,
                m.image as image,
                m.details as details,
                m.trailer_link  as trailerLink,
                m.price  as price,
                m.movie_type  as movieType,
                m.movie_tag  as movieTag,
                m.cast  as cast,
                m.director  as director,
                m.music  as music,
                m.language  as language,
                m.subtitle  as subtitle,
                m.viewed  as view,
                m.url  as url,
                m.meta_author as meta_author,
                m.meta_keywords as meta_keywords,
                m.meta_description as meta_description

            FROM
                ramro_movie as m

            RIGHT JOIN

                ramro_movie_category as c

            ON
                m.cat_id = c.id

            WHERE
                m.status=1
            AND
                m.id = ?
            ";

        DB::query($sql, [$video_id]);
        $result = DB::fetch_assoc();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function UpdateView($video_id) {
        $res = DB::query('UPDATE ramro_movie SET viewed = viewed + ? WHERE id = ?', array(1, $video_id));
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function CheckSubscription($movie_id,$user_id){
	
		
	        $movie = DB::dlookup('movie_type,full_link', 'ramro_movie', 'id=?', array($movie_id));
	
	        switch ($movie['movie_type']) {
	
	            case 'free':
	                return '<button type = "button" class = "btn btn-warning btn-lg video-view" data-vid = "' . $movie['full_link'] . '">
	                <i class = "fa fa-eye"></i> FULL MOVIE </button>';
	                break;
	
	            case 'subscription':
	
	                $checkSubscription = DB::dcount('id', 'ramro_movie_subscription', 'user_id = ? AND status = ?', array($user_id, 1));
	
	                if ($checkSubscription > 0) {
	
	                    return '<button type = "button" class = "btn btn-warning btn-lg video-view" data-vid = "' . $movie['full_link'] . '">
	                <i class = "fa fa-eye"></i> FULL MOVIE </button>';
	                } else {
	                    return '<button <button onclick="location.href=\''.App::urlFor('subscription-message').'\'"  type = "button" class = "btn btn-warning btn-lg"><i class = "fa fa-eye"></i> Subscribe to view</button>';
	                }
	                break;
	
	            case 'premium':
	                $current_time =  new \DateTime();
	                $current_time =  $current_time->format('Y-m-d h:i:s');

                    $checkPremium = DB::dcount('id', 'ramro_movie_premium', 'movie_id = ? AND user_id = ? AND status = ? AND expires_at >= ? ', array($movie_id, $user_id, 1, $current_time));
	                if ($checkPremium > 0) {
                        return '<button type = "button" class = "btn btn-warning btn-lg video-view" data-vid = "' . $movie['full_link'] . '">
	                <i class = "fa fa-eye"></i>VIEW FULL MOVIE </button>';
                    } else {
//	                    return '<button onclick="location.href=\'http://ramro-movie.bidhee.net/payment/'.$movie_id.'\'"  type = "button" class = "btn btn-warning btn-lg"><i class = "fa fa-eye"></i> Buy Premium Movie</button>';
	                    return '<button onclick="location.href=\''.App::urlFor('payment', array('id' => $movie_id)).'\'"  type = "button" class = "btn btn-warning btn-lg"><i class = "fa fa-eye"></i> Buy Premium Movie</button>';
	                }
	                break;
	        }
	    
    }
    
    public static function RelatedVideo($movieType, $except = ''){
    
    	$sql = "SELECT

		DISTINCT
                        m.id as mid,
                        m.title as title,
                        m.image as image,
                        m.details as details,
                        m.trailer_link  as trailerLink,
                        m.full_link  as fullLink,
                        m.price  as price,
                        m.movie_type  as movieType,
                        m.movie_tag  as movieTag,
                        m.cast  as cast,
                        m.director  as director,
                        m.music  as music,
                        m.language  as language,
                        m.subtitle  as subtitle,
                        m.viewed  as view,
                        m.url  as url,
                        m.meta_author as meta_author,
                        m.meta_keywords as meta_keywords,
                        m.meta_description as meta_description

                    FROM
                        ramro_movie as m

                    RIGHT JOIN

                        ramro_movie_category as c

                    ON
                        m.cat_id = c.id

                    WHERE
                        m.status=1
                    AND
                    	m.movie_type=?

                       ";

        if( $except != '' ){
            $sql .= '  AND m.id NOT IN ('.$except.')';
        }

            // DEFAULT ASSIGN ORDER
            $sql .= " ORDER BY m.id DESC LIMIT 25";

            // FINAL QUERY FOR THE RESULT
            DB::query($sql,array($movieType), true);
            return DB::fetch_assoc_all();
    	
    }

}