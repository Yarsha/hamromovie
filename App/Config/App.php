<?php
/*
|=====================================================================================================
| Framework initial configuration
|=====================================================================================================
| you can change according to your use
|
|
*/
DEFINE('SECURE_KEY', substr(SECRET_KEY, 0, 32));


$app = new \Slim\Slim();


// SLIM FCADE ALIASES
require_once 'Aliases.php';
require_once 'Slim.Init.php';

use SlimFacades\Facade;
Facade::setFacadeApplication($app);
Facade::registerAliases($config['aliases']);


App::config($config['init']['slim']);
App::config($config['init']['cookies']);
App::config($config['init']['log']);

// Location:: Application/Config/App.php