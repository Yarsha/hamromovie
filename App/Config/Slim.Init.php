<?php

$config['init']= array(

    'slim' => array(
        'view'                  => new \Slim\Views\Twig(),
        'templates.path'        => APP_FOLDER.'/Modules/Site/Views/',
        'mode'                  => APP_MODE,
        'debug'                 => APP_MODE == 'development' ? true : false
    ),

    'cookies' => array(
        'cookies.secret_key'    => SECURE_KEY,
        'cookies.cipher'        => MCRYPT_RIJNDAEL_256,
        'cookies.encrypt'       => true,
        'cookies.secure'        => true,
        'cookies.lifetime'      => '20 minutes',
        'cookies.httponly'      => true
    ),

    'log' => array(
        'log.enabled'           => APP_MODE == 'production' ? true : false,
        'log.level'             => \Slim\Log::DEBUG,
        'log.writer'            => new \Slim\Extras\Log\DateTimeFileWriter(array(
                                    'path'              => APP_FOLDER.'/Storage/cache/log/',
                                    'name_format'       => 'Y-m-d',
                                    'message_format'    => '%label% - %date% - %message%'))
                                )
);