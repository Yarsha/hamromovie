<?php

/*
  |============================================================================
  | Twig Custom Function
  |============================================================================
  | We can define custom function to use in twig view which add more power to
  | our programming experience
  |
 */



// INITIALIZE TWIG VIEW
$twig = App::view()->getInstance();

/*
  |==============================================================================
  | Flash Message In Twig View
  |==============================================================================
  | Flash Message In Twig View
  |
  |
 */


$CheckMovie = new Twig_SimpleFunction("check_movie", function ($movie_id) {


    $session = New Session();
    $data['logged'] = $session->getSession('SiteloggedIn');

    if ($data['logged'] == true) {

        $user_id = $session->getSession('id');
        $movie = DB::dlookup('movie_type,full_link', 'ramro_movie', 'id=?', array($movie_id));

        switch ($movie['movie_type']) {

            case 'free':
                return '<button type = "button" class = "btn btn-warning btn-lg js-play" data-vid = "' . $movie['full_link'] . '">
                <i class = "fa fa-eye"></i> FULL MOVIE </button>';
                break;

            case 'subscription':

                $checkSubscription = DB::dcount('id', 'ramro_movie_subscription', 'user_id = ? AND status = ?', array($user_id, 1));

                if ($checkSubscription > 0) {

                    return '<button type = "button" class = "btn btn-warning btn-lg js-play" data-vid = "' . $movie['full_link'] . '">
                <i class = "fa fa-eye"></i> FULL MOVIE </button>';
                } else {
                    return '<button type = "button" class = "btn btn-warning btn-lg"><i class = "fa fa-eye"></i> Subscribe to view</button>';
                }
                break;

            case 'premium':

                $checkPremium = DB::dcount('id', 'ramro_movie_premium', 'movie_id = ? AND user_id = ? AND status = ?', array($movie_id, $user_id, 1));
                if ($checkPremium > 0) {

                    return '<button type = "button" class = "btn btn-warning btn-lg js-play" data-vid = "' . $movie['full_link'] . '">
                <i class = "fa fa-eye"></i> FULL MOVIE </button>';
                } else {
                    return '<button type = "button" class = "btn btn-warning btn-lg"><i class = "fa fa-eye"></i>Buy Premium Movie</button>';
                }
                break;
        }
    }
});

$twig->addFunction($CheckMovie);


/*
  |==============================================================================
  | Flash Message In Twig View
  |==============================================================================
  | Flash Message In Twig View
  |
  |
 */


$vimeoVideo = new Twig_SimpleFunction("vimeo", function ($id) {

    $json_string =    file_get_contents("https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/".$id);
    $parsed_json = json_decode($json_string);

    return $parsed_json->thumbnail_url ;
    
});
$twig->addFunction($vimeoVideo);

$flashMessage = new Twig_SimpleFunction("Flash", function ($key) {

    if ($_SESSION['slim.flash'][$key]) {
        return $_SESSION['slim.flash'][$key];
    } else {
        return '';
    }
});
$twig->addFunction($flashMessage);


/*
  |==============================================================================
  | Asset in twig
  |==============================================================================
  | Asset Section in twig templete Engine
  |
  |
 */
$asset = new Twig_SimpleFunction("Asset", function ($link) {

    $rootUri = App::request()->getRootUri();
    return $rootUri . $link;
});
$twig->addFunction($asset);


/*
  |==============================================================================
  | App urlFor
  |==============================================================================
  | Slim urlFor
  |
  |
 */
$asset = new Twig_SimpleFunction("urlFor", function ($path) {

    return App::urlFor($path);
});
$twig->addFunction($asset);

/*
  |==============================================================================
  | Trim text Function in twig
  |==============================================================================
  | Trim text according to need also works fine any unicode
  |
  |
 */
$trimText = new Twig_SimpleFunction("trim_text", function ($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }

    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }

    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);

    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }

    return $trimmed_text;
});
$twig->addFunction($trimText);


/*
  |==============================================================================
  | Relative Data Function
  |==============================================================================
  | Generate date according to user friendly word as 2 days, one hour, yesterday etc
  |
  |
 */
$relativeDate = new Twig_SimpleFunction("relative_date", function ($date) {
    $time = strtotime($date);
    $today = strtotime(date('M j, Y'));

    $reldays = ($time - $today) / 86400;

    if ($reldays >= 0 && $reldays < 1) {
        return 'Today';
    } else if ($reldays >= 1 && $reldays < 2) {
        return 'Tomorrow';
    } else if ($reldays >= -1 && $reldays < 0) {
        return 'Yesterday';
    }

    if (abs($reldays) < 7) {

        if ($reldays > 0) {

            $reldays = floor($reldays);

            return 'In ' . $reldays . ' day' . ($reldays != 1 ? 's' : '');
        } else {

            $reldays = abs(floor($reldays));

            return $reldays . ' day' . ($reldays != 1 ? 's' : '') . ' ago';
        }
    }

    if (abs($reldays) < 182) {

        return date('l, j F', $time ? $time : time());
    } else {

        return date('l, j F, Y', $time ? $time : time());
    }
});
$twig->addFunction($relativeDate);

/*
  |==============================================================================
  | Generate random String
  |==============================================================================
  | We can generate random String to use in code etc.
  |
  |
 */

$randString = new Twig_SimpleFunction("rand_string", function ($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars), 0, $length);
});
$twig->addFunction($randString);


/*
  |==============================================================================
  | Capatilize every First Character of word
  |==============================================================================
  | Uses php native ucWords function in twig
  |
  |
 */
$ucWords = new Twig_SimpleFunction("ucWord", function ($length) {
    return ucWords($length);
});
$twig->addFunction($ucWords);


/*
  |==============================================================================
  | Encrypt the text
  |==============================================================================
  | Rigendel 256 bit encryption used
  |
  |
 */
$encode = new Twig_SimpleFunction("encode", function ($value) {
    if (!empty($value)) {
        $d = New Encryption();
        return $d->encode($value);
    }
});
$twig->addFunction($encode);

/*
  |==============================================================================
  | Decrypt the text
  |==============================================================================
  | Rigendel 256 bit decryption used
  |
  |
 */
$decode = new Twig_SimpleFunction("decode", function ($value) {
    if (!empty($value)) {
        $d = New Encryption();
        return $d->decode($value);
    }
});
$twig->addFunction($decode);


/*
  |==============================================================================
  | Watermark image
  |==============================================================================
  | Watermark image on the fly
  |
  |
 */
$watermark = new Twig_SimpleFunction("watermark", function ($image, $watermarkImage, $marge_right = 10, $marge_bottom = 10) {
    // Load the stamp and the photo to apply the watermark to
    $stamp = imagecreatefrompng($watermarkImage);
    $im = imagecreatefromjpeg($image);

// Set the margins for the stamp and get the height/width of the stamp image
    //$marge_right = 10;
    //$marge_bottom = 10;
    $sx = imagesx($stamp);
    $sy = imagesy($stamp);

// Copy the stamp image onto our photo using the margin offsets and the photo
// width to calculate positioning of the stamp.
    imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

// Output and free memory
    header('Content-type: image/png');
    imagepng($im);
    imagedestroy($im);
});
$twig->addFunction($watermark);