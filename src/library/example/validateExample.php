<?php
$nameErr='';$fileErr='';$emailErr='';$messageErr='';$message_text='';$errors='';
if(isset($_POST['submit'])) {
  
  include('validateClass.php');
  
  //assign post data to variables 
	$name = trim($_POST['name']);
	$email = trim($_POST['email']);
	$message = trim($_POST['message']);

  //start validating our form
  $v = new validate();
  $v->validateNum($name, "First Name", 3, 75);
  $v->validateUrl($email, "Email");
  $v->validateFile('image',array('jpeg', 'jpg', 'gif'),2,'raghu/files/','File');
  $v->validateStr($message, "Message", 5, 8);  
  
  if(!$v->hasErrors()) {
		echo "Success";
	} else {
    //set the number of errors message
    $message_text = $v->errorNumMessage();		

    //store the errors list in a variable
    $errors = $v->displayErrors();
    //get the individual error messages
    $nameErr = $v->getError("First Name");
    $emailErr = $v->getError("Email");
    $messageErr = $v->getError("Message");
    $fileErr = $v->getError("File");
  }//end error check
}// end isset
if(isset($_POST['name'])){$name=$_POST['name'];}else{$name='';};
if(isset($_POST['email'])){$email=$_POST['email'];}else{$email='';};
if(isset($_POST['message'])){$message=$_POST['message'];}else{$message='';};
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, nofollow"/>  
<link href="style.css" rel="stylesheet" type="text/css" />
<title>Simple PHP Form Demo | Box Model Junkie</title>
</head>
<body>
  <div id="contact_form_wrap">
    <span class="message"><?php echo $message_text; ?></span>
    <?php echo $errors; ?>
    <?php if(isset($_GET['sent'])): ?><h2>Your message has been sent</h2><?php endif; ?>
    <form id="contact" method="post" action="" enctype="multipart/form-data">
      <p><label>Name:<br />
      <input type="text" name="name" class="textfield" value="<?php echo htmlentities($name); ?>" />
      </label><br /><span class="errors"><?php echo $nameErr; ?></span></p>
      
      <p><label>Email: <br />
      <input type="text" name="email" class="textfield" value="<?php echo htmlentities($email); ?>" />
      </label><br /><span class="errors"><?php echo $emailErr ?></span></p> 
	  <p><label>upload: <br />
      <input type="file" name="image[]" multiple="multiple" />
      </label><br /><span class="errors"><?php echo $fileErr ?></span></p>          
    
      <p><label>Message: <br />
      <textarea name="message" class="textarea" cols="45" rows="5"><?php echo htmlentities($message); ?></textarea>
      </label><br /><span class="errors"><?php echo $messageErr ?></span></p>
      
      <p><input type="submit" name="submit" class="button" value="Submit" />&nbsp;<input type="reset" class="button" value="Reset" /></p>
    </form>
  </div>
  
</body>
</html>