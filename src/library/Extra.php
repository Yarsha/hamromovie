<?php

/*
 * Date 2014-2-10
 * Version 1.0 
 * Programmar Raghu Chaudhary
 * LIST OF METHODS
 * 1. relative_date($time)
 * 2. trim_text($input, $length, $ellipses = true, $strip_html = true)
 * 3. rand_string($length=4)
 * 4. encrypt($data)
 * 5. decrypt($data)
 */

Class Extra {

    //####################--Random String--######################//
    public function rand_string($length = 4) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    //echo rand_string(6);
    //####################--Random String--######################//
    //####################--Encryption/Decryption--######################//
    public function encrypt($sData) {
        $id = (double) $sData * 98765.8;
        return base64_encode($id);
    }

    public function decrypt($sData) {
        $url_id = base64_decode($sData);
        $id = (double) $url_id / 98765.8;
        return $id;
    }

    //####################--Encryption/Decryption--######################//
    ///#########################<--Relative Date-->####################
    public function relative_date($time) {

        $today = strtotime(date('M j, Y'));

        $reldays = ($time - $today) / 86400;

        if ($reldays >= 0 && $reldays < 1) {

            return 'Today';
        } else if ($reldays >= 1 && $reldays < 2) {

            return 'Tomorrow';
        } else if ($reldays >= -1 && $reldays < 0) {

            return 'Yesterday';
        }

        if (abs($reldays) < 7) {

            if ($reldays > 0) {

                $reldays = floor($reldays);

                return 'In ' . $reldays . ' day' . ($reldays != 1 ? 's' : '');
            } else {

                $reldays = abs(floor($reldays));

                return $reldays . ' day' . ($reldays != 1 ? 's' : '') . ' ago';
            }
        }

        if (abs($reldays) < 182) {

            return date('l, j F', $time ? $time : time());
        } else {

            return date('l, j F, Y', $time ? $time : time());
        }
    }

    /**
     * trims text to a space then adds ellipses if desired
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $strip_html if html tags are to be stripped
     * @return string
     */
    public function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

//####################--HIGHLIGHT FUNCTION--######################//

    public function high_light($str, $keywords = '') {
        $keywords = preg_replace('/\s\s+/', ' ', strip_tags(trim($keywords))); // filter

        $style = 'highlight';
        $style_i = 'highlight_imp';

        /* Apply Style */

        $var = '';

        foreach (explode(' ', $keywords) as $keyword) {
            $replacement = "<span class='" . $style . "'>" . $keyword . "</span>";
            $var .= $replacement . " ";

            $str = str_ireplace($keyword, $replacement, $str);
        }
        $str = str_ireplace(rtrim($var), "<span class='" . $style_i . "'>" . $keywords . "</span>", $str);

        return @$str;
    }

//REMOVE IMAGE FROM CONTENT
    function remove_image($content) {
        $content = preg_replace("/<img[^>]+\>/i", "", $content);
        return $content;
    }

}

?>