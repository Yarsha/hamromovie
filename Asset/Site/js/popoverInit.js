var options = {
        placement: function (context, source) {
            var $win, $source, winWidth, popoverWidth, popoverHeight, offset, toRight, toLeft, placement, scrollTop;

            $win = $(window);
            $source = $(source);
            placement = $source.attr('data-placement');
            popoverWidth = 400;
            popoverHeight = 110;
            offset = $source.offset();

            // Check for horizontal positioning and try to use it.
            if (placement.match(/^right|left$/)) {
                winWidth = $win.width();
                toRight = winWidth - offset.left - source.offsetWidth;
                toLeft = offset.left;

                if (placement === 'left') {
                    if (toLeft > popoverWidth) {
                        return 'left';
                    }
                    else if (toRight > popoverWidth) {
                        return 'right';
                    }
                }
                else {
                    if (toRight > popoverWidth) {
                        return 'right';
                    }
                    else if (toLeft > popoverWidth) {
                        return 'left';
                    }
                }
            }

            // Handle vertical positioning.
            scrollTop = $win.scrollTop();
            if (placement === 'bottom') {
                if (($win.height() + scrollTop) - (offset.top + source.offsetHeight) > popoverHeight) {
                    return 'bottom';
                }
                return 'top';
            }
            else {
                if (offset.top - scrollTop > popoverHeight) {
                    return 'top';
                }
                return 'bottom';
            }
        },
        trigger: 'hover'
    };
    $('[rel="popover"]').popover(options);