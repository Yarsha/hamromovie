<?php

//echo  phpinfo(); die;

/*
|-----------------------------------------------
| WEB MASTER RNC
| Modified: 2015-FEB-02
| Mail: raghu.kataria@gmail.com
|-----------------------------------------------
*/


ob_start();
session_cache_limiter(false);
session_start();

/*
|-----------------------------------------------
| System Setting
|-----------------------------------------------
*/

error_reporting(E_ALL | E_STRICT);
error_reporting(error_reporting() & ~E_NOTICE);
date_default_timezone_set('UTC');

DEFINE('ROOT',dirname(__FILE__).'/');
//echo ROOT; die;
include ROOT . 'App/Config/Constants.php';
include ROOT . 'vendor/autoload.php';
include APP_FOLDER . '/Start.php';
App::run();
